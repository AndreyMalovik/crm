<?php

use Base\TrustToUserPremiseQuery as BaseTrustToUserPremiseQuery;

/**
 * Skeleton subclass for performing query and update operations on the 'trust_to_user_premise' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class TrustToUserPremiseQuery extends BaseTrustToUserPremiseQuery
{

}
