<?php

use Base\RoutesQuery as BaseRoutesQuery;

/**
 * Skeleton subclass for performing query and update operations on the 'routes' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class RoutesQuery extends BaseRoutesQuery
{

}
