<?php

namespace Map;

use \Premises;
use \PremisesQuery;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\InstancePoolTrait;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\DataFetcher\DataFetcherInterface;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\RelationMap;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Map\TableMapTrait;


/**
 * This class defines the structure of the 'premises' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 */
class PremisesTableMap extends TableMap
{
    use InstancePoolTrait;
    use TableMapTrait;

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = '.Map.PremisesTableMap';

    /**
     * The default database name for this class
     */
    const DATABASE_NAME = 'crm';

    /**
     * The table name for this class
     */
    const TABLE_NAME = 'premises';

    /**
     * The related Propel class for this table
     */
    const OM_CLASS = '\\Premises';

    /**
     * A class that can be returned by this tableMap
     */
    const CLASS_DEFAULT = 'Premises';

    /**
     * The total number of columns
     */
    const NUM_COLUMNS = 12;

    /**
     * The number of lazy-loaded columns
     */
    const NUM_LAZY_LOAD_COLUMNS = 0;

    /**
     * The number of columns to hydrate (NUM_COLUMNS - NUM_LAZY_LOAD_COLUMNS)
     */
    const NUM_HYDRATE_COLUMNS = 12;

    /**
     * the column name for the id field
     */
    const COL_ID = 'premises.id';

    /**
     * the column name for the date_sender field
     */
    const COL_DATE_SENDER = 'premises.date_sender';

    /**
     * the column name for the date_recipient field
     */
    const COL_DATE_RECIPIENT = 'premises.date_recipient';

    /**
     * the column name for the type_id field
     */
    const COL_TYPE_ID = 'premises.type_id';

    /**
     * the column name for the count field
     */
    const COL_COUNT = 'premises.count';

    /**
     * the column name for the route_id field
     */
    const COL_ROUTE_ID = 'premises.route_id';

    /**
     * the column name for the cost field
     */
    const COL_COST = 'premises.cost';

    /**
     * the column name for the city_sender field
     */
    const COL_CITY_SENDER = 'premises.city_sender';

    /**
     * the column name for the city_recipient field
     */
    const COL_CITY_RECIPIENT = 'premises.city_recipient';

    /**
     * the column name for the status_id field
     */
    const COL_STATUS_ID = 'premises.status_id';

    /**
     * the column name for the payer_id field
     */
    const COL_PAYER_ID = 'premises.payer_id';

    /**
     * the column name for the comment field
     */
    const COL_COMMENT = 'premises.comment';

    /**
     * The default string format for model objects of the related table
     */
    const DEFAULT_STRING_FORMAT = 'YAML';

    /**
     * holds an array of fieldnames
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldNames[self::TYPE_PHPNAME][0] = 'Id'
     */
    protected static $fieldNames = array (
        self::TYPE_PHPNAME       => array('Id', 'DateSender', 'DateRecipient', 'TypeId', 'Count', 'RouteId', 'Cost', 'CitySender', 'CityRecipient', 'StatusId', 'PayerId', 'Comment', ),
        self::TYPE_CAMELNAME     => array('id', 'dateSender', 'dateRecipient', 'typeId', 'count', 'routeId', 'cost', 'citySender', 'cityRecipient', 'statusId', 'payerId', 'comment', ),
        self::TYPE_COLNAME       => array(PremisesTableMap::COL_ID, PremisesTableMap::COL_DATE_SENDER, PremisesTableMap::COL_DATE_RECIPIENT, PremisesTableMap::COL_TYPE_ID, PremisesTableMap::COL_COUNT, PremisesTableMap::COL_ROUTE_ID, PremisesTableMap::COL_COST, PremisesTableMap::COL_CITY_SENDER, PremisesTableMap::COL_CITY_RECIPIENT, PremisesTableMap::COL_STATUS_ID, PremisesTableMap::COL_PAYER_ID, PremisesTableMap::COL_COMMENT, ),
        self::TYPE_FIELDNAME     => array('id', 'date_sender', 'date_recipient', 'type_id', 'count', 'route_id', 'cost', 'city_sender', 'city_recipient', 'status_id', 'payer_id', 'comment', ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, )
    );

    /**
     * holds an array of keys for quick access to the fieldnames array
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldKeys[self::TYPE_PHPNAME]['Id'] = 0
     */
    protected static $fieldKeys = array (
        self::TYPE_PHPNAME       => array('Id' => 0, 'DateSender' => 1, 'DateRecipient' => 2, 'TypeId' => 3, 'Count' => 4, 'RouteId' => 5, 'Cost' => 6, 'CitySender' => 7, 'CityRecipient' => 8, 'StatusId' => 9, 'PayerId' => 10, 'Comment' => 11, ),
        self::TYPE_CAMELNAME     => array('id' => 0, 'dateSender' => 1, 'dateRecipient' => 2, 'typeId' => 3, 'count' => 4, 'routeId' => 5, 'cost' => 6, 'citySender' => 7, 'cityRecipient' => 8, 'statusId' => 9, 'payerId' => 10, 'comment' => 11, ),
        self::TYPE_COLNAME       => array(PremisesTableMap::COL_ID => 0, PremisesTableMap::COL_DATE_SENDER => 1, PremisesTableMap::COL_DATE_RECIPIENT => 2, PremisesTableMap::COL_TYPE_ID => 3, PremisesTableMap::COL_COUNT => 4, PremisesTableMap::COL_ROUTE_ID => 5, PremisesTableMap::COL_COST => 6, PremisesTableMap::COL_CITY_SENDER => 7, PremisesTableMap::COL_CITY_RECIPIENT => 8, PremisesTableMap::COL_STATUS_ID => 9, PremisesTableMap::COL_PAYER_ID => 10, PremisesTableMap::COL_COMMENT => 11, ),
        self::TYPE_FIELDNAME     => array('id' => 0, 'date_sender' => 1, 'date_recipient' => 2, 'type_id' => 3, 'count' => 4, 'route_id' => 5, 'cost' => 6, 'city_sender' => 7, 'city_recipient' => 8, 'status_id' => 9, 'payer_id' => 10, 'comment' => 11, ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, )
    );

    /**
     * Initialize the table attributes and columns
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('premises');
        $this->setPhpName('Premises');
        $this->setIdentifierQuoting(false);
        $this->setClassName('\\Premises');
        $this->setPackage('');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('id', 'Id', 'BIGINT', true, null, null);
        $this->addColumn('date_sender', 'DateSender', 'BIGINT', true, null, null);
        $this->addColumn('date_recipient', 'DateRecipient', 'INTEGER', true, null, null);
        $this->addForeignKey('type_id', 'TypeId', 'TINYINT', 'types_premise', 'id', true, 3, null);
        $this->addColumn('count', 'Count', 'TINYINT', true, 3, null);
        $this->addForeignKey('route_id', 'RouteId', 'BIGINT', 'routes', 'id', true, null, null);
        $this->addColumn('cost', 'Cost', 'INTEGER', true, 10, null);
        $this->addColumn('city_sender', 'CitySender', 'VARCHAR', true, 255, null);
        $this->addColumn('city_recipient', 'CityRecipient', 'VARCHAR', true, 255, null);
        $this->addForeignKey('status_id', 'StatusId', 'TINYINT', 'statuses', 'id', true, 3, null);
        $this->addForeignKey('payer_id', 'PayerId', 'BIGINT', 'users', 'id', true, null, null);
        $this->addColumn('comment', 'Comment', 'LONGVARCHAR', true, null, null);
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('Users', '\\Users', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':payer_id',
    1 => ':id',
  ),
), null, null, null, false);
        $this->addRelation('Routes', '\\Routes', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':route_id',
    1 => ':id',
  ),
), null, null, null, false);
        $this->addRelation('Statuses', '\\Statuses', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':status_id',
    1 => ':id',
  ),
), null, null, null, false);
        $this->addRelation('TypesPremise', '\\TypesPremise', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':type_id',
    1 => ':id',
  ),
), null, null, null, false);
        $this->addRelation('UserToPremises', '\\UserToPremises', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':premise_id',
    1 => ':id',
  ),
), null, null, 'UserToPremisess', false);
    } // buildRelations()

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return string The primary key hash of the row
     */
    public static function getPrimaryKeyHashFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        // If the PK cannot be derived from the row, return NULL.
        if ($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] === null) {
            return null;
        }

        return null === $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] || is_scalar($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)]) || is_callable([$row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)], '__toString']) ? (string) $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] : $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)];
    }

    /**
     * Retrieves the primary key from the DB resultset row
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, an array of the primary key columns will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return mixed The primary key of the row
     */
    public static function getPrimaryKeyFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        return (string) $row[
            $indexType == TableMap::TYPE_NUM
                ? 0 + $offset
                : self::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)
        ];
    }

    /**
     * The class that the tableMap will make instances of.
     *
     * If $withPrefix is true, the returned path
     * uses a dot-path notation which is translated into a path
     * relative to a location on the PHP include_path.
     * (e.g. path.to.MyClass -> 'path/to/MyClass.php')
     *
     * @param boolean $withPrefix Whether or not to return the path with the class name
     * @return string path.to.ClassName
     */
    public static function getOMClass($withPrefix = true)
    {
        return $withPrefix ? PremisesTableMap::CLASS_DEFAULT : PremisesTableMap::OM_CLASS;
    }

    /**
     * Populates an object of the default type or an object that inherit from the default.
     *
     * @param array  $row       row returned by DataFetcher->fetch().
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                 One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     * @return array           (Premises object, last column rank)
     */
    public static function populateObject($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        $key = PremisesTableMap::getPrimaryKeyHashFromRow($row, $offset, $indexType);
        if (null !== ($obj = PremisesTableMap::getInstanceFromPool($key))) {
            // We no longer rehydrate the object, since this can cause data loss.
            // See http://www.propelorm.org/ticket/509
            // $obj->hydrate($row, $offset, true); // rehydrate
            $col = $offset + PremisesTableMap::NUM_HYDRATE_COLUMNS;
        } else {
            $cls = PremisesTableMap::OM_CLASS;
            /** @var Premises $obj */
            $obj = new $cls();
            $col = $obj->hydrate($row, $offset, false, $indexType);
            PremisesTableMap::addInstanceToPool($obj, $key);
        }

        return array($obj, $col);
    }

    /**
     * The returned array will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @param DataFetcherInterface $dataFetcher
     * @return array
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function populateObjects(DataFetcherInterface $dataFetcher)
    {
        $results = array();

        // set the class once to avoid overhead in the loop
        $cls = static::getOMClass(false);
        // populate the object(s)
        while ($row = $dataFetcher->fetch()) {
            $key = PremisesTableMap::getPrimaryKeyHashFromRow($row, 0, $dataFetcher->getIndexType());
            if (null !== ($obj = PremisesTableMap::getInstanceFromPool($key))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj->hydrate($row, 0, true); // rehydrate
                $results[] = $obj;
            } else {
                /** @var Premises $obj */
                $obj = new $cls();
                $obj->hydrate($row);
                $results[] = $obj;
                PremisesTableMap::addInstanceToPool($obj, $key);
            } // if key exists
        }

        return $results;
    }
    /**
     * Add all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be added to the select list and only loaded
     * on demand.
     *
     * @param Criteria $criteria object containing the columns to add.
     * @param string   $alias    optional table alias
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function addSelectColumns(Criteria $criteria, $alias = null)
    {
        if (null === $alias) {
            $criteria->addSelectColumn(PremisesTableMap::COL_ID);
            $criteria->addSelectColumn(PremisesTableMap::COL_DATE_SENDER);
            $criteria->addSelectColumn(PremisesTableMap::COL_DATE_RECIPIENT);
            $criteria->addSelectColumn(PremisesTableMap::COL_TYPE_ID);
            $criteria->addSelectColumn(PremisesTableMap::COL_COUNT);
            $criteria->addSelectColumn(PremisesTableMap::COL_ROUTE_ID);
            $criteria->addSelectColumn(PremisesTableMap::COL_COST);
            $criteria->addSelectColumn(PremisesTableMap::COL_CITY_SENDER);
            $criteria->addSelectColumn(PremisesTableMap::COL_CITY_RECIPIENT);
            $criteria->addSelectColumn(PremisesTableMap::COL_STATUS_ID);
            $criteria->addSelectColumn(PremisesTableMap::COL_PAYER_ID);
            $criteria->addSelectColumn(PremisesTableMap::COL_COMMENT);
        } else {
            $criteria->addSelectColumn($alias . '.id');
            $criteria->addSelectColumn($alias . '.date_sender');
            $criteria->addSelectColumn($alias . '.date_recipient');
            $criteria->addSelectColumn($alias . '.type_id');
            $criteria->addSelectColumn($alias . '.count');
            $criteria->addSelectColumn($alias . '.route_id');
            $criteria->addSelectColumn($alias . '.cost');
            $criteria->addSelectColumn($alias . '.city_sender');
            $criteria->addSelectColumn($alias . '.city_recipient');
            $criteria->addSelectColumn($alias . '.status_id');
            $criteria->addSelectColumn($alias . '.payer_id');
            $criteria->addSelectColumn($alias . '.comment');
        }
    }

    /**
     * Returns the TableMap related to this object.
     * This method is not needed for general use but a specific application could have a need.
     * @return TableMap
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function getTableMap()
    {
        return Propel::getServiceContainer()->getDatabaseMap(PremisesTableMap::DATABASE_NAME)->getTable(PremisesTableMap::TABLE_NAME);
    }

    /**
     * Add a TableMap instance to the database for this tableMap class.
     */
    public static function buildTableMap()
    {
        $dbMap = Propel::getServiceContainer()->getDatabaseMap(PremisesTableMap::DATABASE_NAME);
        if (!$dbMap->hasTable(PremisesTableMap::TABLE_NAME)) {
            $dbMap->addTableObject(new PremisesTableMap());
        }
    }

    /**
     * Performs a DELETE on the database, given a Premises or Criteria object OR a primary key value.
     *
     * @param mixed               $values Criteria or Premises object or primary key or array of primary keys
     *              which is used to create the DELETE statement
     * @param  ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
     public static function doDelete($values, ConnectionInterface $con = null)
     {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(PremisesTableMap::DATABASE_NAME);
        }

        if ($values instanceof Criteria) {
            // rename for clarity
            $criteria = $values;
        } elseif ($values instanceof \Premises) { // it's a model object
            // create criteria based on pk values
            $criteria = $values->buildPkeyCriteria();
        } else { // it's a primary key, or an array of pks
            $criteria = new Criteria(PremisesTableMap::DATABASE_NAME);
            $criteria->add(PremisesTableMap::COL_ID, (array) $values, Criteria::IN);
        }

        $query = PremisesQuery::create()->mergeWith($criteria);

        if ($values instanceof Criteria) {
            PremisesTableMap::clearInstancePool();
        } elseif (!is_object($values)) { // it's a primary key, or an array of pks
            foreach ((array) $values as $singleval) {
                PremisesTableMap::removeInstanceFromPool($singleval);
            }
        }

        return $query->delete($con);
    }

    /**
     * Deletes all rows from the premises table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public static function doDeleteAll(ConnectionInterface $con = null)
    {
        return PremisesQuery::create()->doDeleteAll($con);
    }

    /**
     * Performs an INSERT on the database, given a Premises or Criteria object.
     *
     * @param mixed               $criteria Criteria or Premises object containing data that is used to create the INSERT statement.
     * @param ConnectionInterface $con the ConnectionInterface connection to use
     * @return mixed           The new primary key.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function doInsert($criteria, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(PremisesTableMap::DATABASE_NAME);
        }

        if ($criteria instanceof Criteria) {
            $criteria = clone $criteria; // rename for clarity
        } else {
            $criteria = $criteria->buildCriteria(); // build Criteria from Premises object
        }

        if ($criteria->containsKey(PremisesTableMap::COL_ID) && $criteria->keyContainsValue(PremisesTableMap::COL_ID) ) {
            throw new PropelException('Cannot insert a value for auto-increment primary key ('.PremisesTableMap::COL_ID.')');
        }


        // Set the correct dbName
        $query = PremisesQuery::create()->mergeWith($criteria);

        // use transaction because $criteria could contain info
        // for more than one table (I guess, conceivably)
        return $con->transaction(function () use ($con, $query) {
            return $query->doInsert($con);
        });
    }

} // PremisesTableMap
// This is the static code needed to register the TableMap for this table with the main Propel class.
//
PremisesTableMap::buildTableMap();
