<?php

use Base\TrustToUserPremise as BaseTrustToUserPremise;

/**
 * Skeleton subclass for representing a row from the 'trust_to_user_premise' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class TrustToUserPremise extends BaseTrustToUserPremise
{

}
