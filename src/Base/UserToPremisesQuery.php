<?php

namespace Base;

use \UserToPremises as ChildUserToPremises;
use \UserToPremisesQuery as ChildUserToPremisesQuery;
use \Exception;
use \PDO;
use Map\UserToPremisesTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveQuery\ModelJoin;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'user_to_premises' table.
 *
 *
 *
 * @method     ChildUserToPremisesQuery orderById($order = Criteria::ASC) Order by the id column
 * @method     ChildUserToPremisesQuery orderByUserSender($order = Criteria::ASC) Order by the user_sender column
 * @method     ChildUserToPremisesQuery orderByPremiseId($order = Criteria::ASC) Order by the premise_id column
 * @method     ChildUserToPremisesQuery orderByUserRecipient($order = Criteria::ASC) Order by the user_recipient column
 * @method     ChildUserToPremisesQuery orderByTrustUserId($order = Criteria::ASC) Order by the trust_user_id column
 *
 * @method     ChildUserToPremisesQuery groupById() Group by the id column
 * @method     ChildUserToPremisesQuery groupByUserSender() Group by the user_sender column
 * @method     ChildUserToPremisesQuery groupByPremiseId() Group by the premise_id column
 * @method     ChildUserToPremisesQuery groupByUserRecipient() Group by the user_recipient column
 * @method     ChildUserToPremisesQuery groupByTrustUserId() Group by the trust_user_id column
 *
 * @method     ChildUserToPremisesQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildUserToPremisesQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildUserToPremisesQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildUserToPremisesQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildUserToPremisesQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildUserToPremisesQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildUserToPremisesQuery leftJoinPremises($relationAlias = null) Adds a LEFT JOIN clause to the query using the Premises relation
 * @method     ChildUserToPremisesQuery rightJoinPremises($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Premises relation
 * @method     ChildUserToPremisesQuery innerJoinPremises($relationAlias = null) Adds a INNER JOIN clause to the query using the Premises relation
 *
 * @method     ChildUserToPremisesQuery joinWithPremises($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Premises relation
 *
 * @method     ChildUserToPremisesQuery leftJoinWithPremises() Adds a LEFT JOIN clause and with to the query using the Premises relation
 * @method     ChildUserToPremisesQuery rightJoinWithPremises() Adds a RIGHT JOIN clause and with to the query using the Premises relation
 * @method     ChildUserToPremisesQuery innerJoinWithPremises() Adds a INNER JOIN clause and with to the query using the Premises relation
 *
 * @method     ChildUserToPremisesQuery leftJoinUsersRelatedByTrustUserId($relationAlias = null) Adds a LEFT JOIN clause to the query using the UsersRelatedByTrustUserId relation
 * @method     ChildUserToPremisesQuery rightJoinUsersRelatedByTrustUserId($relationAlias = null) Adds a RIGHT JOIN clause to the query using the UsersRelatedByTrustUserId relation
 * @method     ChildUserToPremisesQuery innerJoinUsersRelatedByTrustUserId($relationAlias = null) Adds a INNER JOIN clause to the query using the UsersRelatedByTrustUserId relation
 *
 * @method     ChildUserToPremisesQuery joinWithUsersRelatedByTrustUserId($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the UsersRelatedByTrustUserId relation
 *
 * @method     ChildUserToPremisesQuery leftJoinWithUsersRelatedByTrustUserId() Adds a LEFT JOIN clause and with to the query using the UsersRelatedByTrustUserId relation
 * @method     ChildUserToPremisesQuery rightJoinWithUsersRelatedByTrustUserId() Adds a RIGHT JOIN clause and with to the query using the UsersRelatedByTrustUserId relation
 * @method     ChildUserToPremisesQuery innerJoinWithUsersRelatedByTrustUserId() Adds a INNER JOIN clause and with to the query using the UsersRelatedByTrustUserId relation
 *
 * @method     ChildUserToPremisesQuery leftJoinUsersRelatedByUserRecipient($relationAlias = null) Adds a LEFT JOIN clause to the query using the UsersRelatedByUserRecipient relation
 * @method     ChildUserToPremisesQuery rightJoinUsersRelatedByUserRecipient($relationAlias = null) Adds a RIGHT JOIN clause to the query using the UsersRelatedByUserRecipient relation
 * @method     ChildUserToPremisesQuery innerJoinUsersRelatedByUserRecipient($relationAlias = null) Adds a INNER JOIN clause to the query using the UsersRelatedByUserRecipient relation
 *
 * @method     ChildUserToPremisesQuery joinWithUsersRelatedByUserRecipient($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the UsersRelatedByUserRecipient relation
 *
 * @method     ChildUserToPremisesQuery leftJoinWithUsersRelatedByUserRecipient() Adds a LEFT JOIN clause and with to the query using the UsersRelatedByUserRecipient relation
 * @method     ChildUserToPremisesQuery rightJoinWithUsersRelatedByUserRecipient() Adds a RIGHT JOIN clause and with to the query using the UsersRelatedByUserRecipient relation
 * @method     ChildUserToPremisesQuery innerJoinWithUsersRelatedByUserRecipient() Adds a INNER JOIN clause and with to the query using the UsersRelatedByUserRecipient relation
 *
 * @method     ChildUserToPremisesQuery leftJoinUsersRelatedByUserSender($relationAlias = null) Adds a LEFT JOIN clause to the query using the UsersRelatedByUserSender relation
 * @method     ChildUserToPremisesQuery rightJoinUsersRelatedByUserSender($relationAlias = null) Adds a RIGHT JOIN clause to the query using the UsersRelatedByUserSender relation
 * @method     ChildUserToPremisesQuery innerJoinUsersRelatedByUserSender($relationAlias = null) Adds a INNER JOIN clause to the query using the UsersRelatedByUserSender relation
 *
 * @method     ChildUserToPremisesQuery joinWithUsersRelatedByUserSender($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the UsersRelatedByUserSender relation
 *
 * @method     ChildUserToPremisesQuery leftJoinWithUsersRelatedByUserSender() Adds a LEFT JOIN clause and with to the query using the UsersRelatedByUserSender relation
 * @method     ChildUserToPremisesQuery rightJoinWithUsersRelatedByUserSender() Adds a RIGHT JOIN clause and with to the query using the UsersRelatedByUserSender relation
 * @method     ChildUserToPremisesQuery innerJoinWithUsersRelatedByUserSender() Adds a INNER JOIN clause and with to the query using the UsersRelatedByUserSender relation
 *
 * @method     \PremisesQuery|\UsersQuery endUse() Finalizes a secondary criteria and merges it with its primary Criteria
 *
 * @method     ChildUserToPremises findOne(ConnectionInterface $con = null) Return the first ChildUserToPremises matching the query
 * @method     ChildUserToPremises findOneOrCreate(ConnectionInterface $con = null) Return the first ChildUserToPremises matching the query, or a new ChildUserToPremises object populated from the query conditions when no match is found
 *
 * @method     ChildUserToPremises findOneById(string $id) Return the first ChildUserToPremises filtered by the id column
 * @method     ChildUserToPremises findOneByUserSender(string $user_sender) Return the first ChildUserToPremises filtered by the user_sender column
 * @method     ChildUserToPremises findOneByPremiseId(string $premise_id) Return the first ChildUserToPremises filtered by the premise_id column
 * @method     ChildUserToPremises findOneByUserRecipient(string $user_recipient) Return the first ChildUserToPremises filtered by the user_recipient column
 * @method     ChildUserToPremises findOneByTrustUserId(string $trust_user_id) Return the first ChildUserToPremises filtered by the trust_user_id column *

 * @method     ChildUserToPremises requirePk($key, ConnectionInterface $con = null) Return the ChildUserToPremises by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildUserToPremises requireOne(ConnectionInterface $con = null) Return the first ChildUserToPremises matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildUserToPremises requireOneById(string $id) Return the first ChildUserToPremises filtered by the id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildUserToPremises requireOneByUserSender(string $user_sender) Return the first ChildUserToPremises filtered by the user_sender column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildUserToPremises requireOneByPremiseId(string $premise_id) Return the first ChildUserToPremises filtered by the premise_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildUserToPremises requireOneByUserRecipient(string $user_recipient) Return the first ChildUserToPremises filtered by the user_recipient column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildUserToPremises requireOneByTrustUserId(string $trust_user_id) Return the first ChildUserToPremises filtered by the trust_user_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildUserToPremises[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildUserToPremises objects based on current ModelCriteria
 * @method     ChildUserToPremises[]|ObjectCollection findById(string $id) Return ChildUserToPremises objects filtered by the id column
 * @method     ChildUserToPremises[]|ObjectCollection findByUserSender(string $user_sender) Return ChildUserToPremises objects filtered by the user_sender column
 * @method     ChildUserToPremises[]|ObjectCollection findByPremiseId(string $premise_id) Return ChildUserToPremises objects filtered by the premise_id column
 * @method     ChildUserToPremises[]|ObjectCollection findByUserRecipient(string $user_recipient) Return ChildUserToPremises objects filtered by the user_recipient column
 * @method     ChildUserToPremises[]|ObjectCollection findByTrustUserId(string $trust_user_id) Return ChildUserToPremises objects filtered by the trust_user_id column
 * @method     ChildUserToPremises[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class UserToPremisesQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \Base\UserToPremisesQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'crm', $modelName = '\\UserToPremises', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildUserToPremisesQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildUserToPremisesQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildUserToPremisesQuery) {
            return $criteria;
        }
        $query = new ChildUserToPremisesQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildUserToPremises|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(UserToPremisesTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = UserToPremisesTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildUserToPremises A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT id, user_sender, premise_id, user_recipient, trust_user_id FROM user_to_premises WHERE id = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildUserToPremises $obj */
            $obj = new ChildUserToPremises();
            $obj->hydrate($row);
            UserToPremisesTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildUserToPremises|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildUserToPremisesQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(UserToPremisesTableMap::COL_ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildUserToPremisesQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(UserToPremisesTableMap::COL_ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE id = 1234
     * $query->filterById(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE id > 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildUserToPremisesQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(UserToPremisesTableMap::COL_ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(UserToPremisesTableMap::COL_ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(UserToPremisesTableMap::COL_ID, $id, $comparison);
    }

    /**
     * Filter the query on the user_sender column
     *
     * Example usage:
     * <code>
     * $query->filterByUserSender(1234); // WHERE user_sender = 1234
     * $query->filterByUserSender(array(12, 34)); // WHERE user_sender IN (12, 34)
     * $query->filterByUserSender(array('min' => 12)); // WHERE user_sender > 12
     * </code>
     *
     * @see       filterByUsersRelatedByUserSender()
     *
     * @param     mixed $userSender The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildUserToPremisesQuery The current query, for fluid interface
     */
    public function filterByUserSender($userSender = null, $comparison = null)
    {
        if (is_array($userSender)) {
            $useMinMax = false;
            if (isset($userSender['min'])) {
                $this->addUsingAlias(UserToPremisesTableMap::COL_USER_SENDER, $userSender['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($userSender['max'])) {
                $this->addUsingAlias(UserToPremisesTableMap::COL_USER_SENDER, $userSender['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(UserToPremisesTableMap::COL_USER_SENDER, $userSender, $comparison);
    }

    /**
     * Filter the query on the premise_id column
     *
     * Example usage:
     * <code>
     * $query->filterByPremiseId(1234); // WHERE premise_id = 1234
     * $query->filterByPremiseId(array(12, 34)); // WHERE premise_id IN (12, 34)
     * $query->filterByPremiseId(array('min' => 12)); // WHERE premise_id > 12
     * </code>
     *
     * @see       filterByPremises()
     *
     * @param     mixed $premiseId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildUserToPremisesQuery The current query, for fluid interface
     */
    public function filterByPremiseId($premiseId = null, $comparison = null)
    {
        if (is_array($premiseId)) {
            $useMinMax = false;
            if (isset($premiseId['min'])) {
                $this->addUsingAlias(UserToPremisesTableMap::COL_PREMISE_ID, $premiseId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($premiseId['max'])) {
                $this->addUsingAlias(UserToPremisesTableMap::COL_PREMISE_ID, $premiseId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(UserToPremisesTableMap::COL_PREMISE_ID, $premiseId, $comparison);
    }

    /**
     * Filter the query on the user_recipient column
     *
     * Example usage:
     * <code>
     * $query->filterByUserRecipient(1234); // WHERE user_recipient = 1234
     * $query->filterByUserRecipient(array(12, 34)); // WHERE user_recipient IN (12, 34)
     * $query->filterByUserRecipient(array('min' => 12)); // WHERE user_recipient > 12
     * </code>
     *
     * @see       filterByUsersRelatedByUserRecipient()
     *
     * @param     mixed $userRecipient The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildUserToPremisesQuery The current query, for fluid interface
     */
    public function filterByUserRecipient($userRecipient = null, $comparison = null)
    {
        if (is_array($userRecipient)) {
            $useMinMax = false;
            if (isset($userRecipient['min'])) {
                $this->addUsingAlias(UserToPremisesTableMap::COL_USER_RECIPIENT, $userRecipient['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($userRecipient['max'])) {
                $this->addUsingAlias(UserToPremisesTableMap::COL_USER_RECIPIENT, $userRecipient['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(UserToPremisesTableMap::COL_USER_RECIPIENT, $userRecipient, $comparison);
    }

    /**
     * Filter the query on the trust_user_id column
     *
     * Example usage:
     * <code>
     * $query->filterByTrustUserId(1234); // WHERE trust_user_id = 1234
     * $query->filterByTrustUserId(array(12, 34)); // WHERE trust_user_id IN (12, 34)
     * $query->filterByTrustUserId(array('min' => 12)); // WHERE trust_user_id > 12
     * </code>
     *
     * @see       filterByUsersRelatedByTrustUserId()
     *
     * @param     mixed $trustUserId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildUserToPremisesQuery The current query, for fluid interface
     */
    public function filterByTrustUserId($trustUserId = null, $comparison = null)
    {
        if (is_array($trustUserId)) {
            $useMinMax = false;
            if (isset($trustUserId['min'])) {
                $this->addUsingAlias(UserToPremisesTableMap::COL_TRUST_USER_ID, $trustUserId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($trustUserId['max'])) {
                $this->addUsingAlias(UserToPremisesTableMap::COL_TRUST_USER_ID, $trustUserId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(UserToPremisesTableMap::COL_TRUST_USER_ID, $trustUserId, $comparison);
    }

    /**
     * Filter the query by a related \Premises object
     *
     * @param \Premises|ObjectCollection $premises The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildUserToPremisesQuery The current query, for fluid interface
     */
    public function filterByPremises($premises, $comparison = null)
    {
        if ($premises instanceof \Premises) {
            return $this
                ->addUsingAlias(UserToPremisesTableMap::COL_PREMISE_ID, $premises->getId(), $comparison);
        } elseif ($premises instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(UserToPremisesTableMap::COL_PREMISE_ID, $premises->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByPremises() only accepts arguments of type \Premises or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Premises relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildUserToPremisesQuery The current query, for fluid interface
     */
    public function joinPremises($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Premises');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Premises');
        }

        return $this;
    }

    /**
     * Use the Premises relation Premises object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \PremisesQuery A secondary query class using the current class as primary query
     */
    public function usePremisesQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinPremises($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Premises', '\PremisesQuery');
    }

    /**
     * Filter the query by a related \Users object
     *
     * @param \Users|ObjectCollection $users The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildUserToPremisesQuery The current query, for fluid interface
     */
    public function filterByUsersRelatedByTrustUserId($users, $comparison = null)
    {
        if ($users instanceof \Users) {
            return $this
                ->addUsingAlias(UserToPremisesTableMap::COL_TRUST_USER_ID, $users->getId(), $comparison);
        } elseif ($users instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(UserToPremisesTableMap::COL_TRUST_USER_ID, $users->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByUsersRelatedByTrustUserId() only accepts arguments of type \Users or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the UsersRelatedByTrustUserId relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildUserToPremisesQuery The current query, for fluid interface
     */
    public function joinUsersRelatedByTrustUserId($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('UsersRelatedByTrustUserId');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'UsersRelatedByTrustUserId');
        }

        return $this;
    }

    /**
     * Use the UsersRelatedByTrustUserId relation Users object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \UsersQuery A secondary query class using the current class as primary query
     */
    public function useUsersRelatedByTrustUserIdQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinUsersRelatedByTrustUserId($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'UsersRelatedByTrustUserId', '\UsersQuery');
    }

    /**
     * Filter the query by a related \Users object
     *
     * @param \Users|ObjectCollection $users The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildUserToPremisesQuery The current query, for fluid interface
     */
    public function filterByUsersRelatedByUserRecipient($users, $comparison = null)
    {
        if ($users instanceof \Users) {
            return $this
                ->addUsingAlias(UserToPremisesTableMap::COL_USER_RECIPIENT, $users->getId(), $comparison);
        } elseif ($users instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(UserToPremisesTableMap::COL_USER_RECIPIENT, $users->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByUsersRelatedByUserRecipient() only accepts arguments of type \Users or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the UsersRelatedByUserRecipient relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildUserToPremisesQuery The current query, for fluid interface
     */
    public function joinUsersRelatedByUserRecipient($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('UsersRelatedByUserRecipient');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'UsersRelatedByUserRecipient');
        }

        return $this;
    }

    /**
     * Use the UsersRelatedByUserRecipient relation Users object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \UsersQuery A secondary query class using the current class as primary query
     */
    public function useUsersRelatedByUserRecipientQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinUsersRelatedByUserRecipient($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'UsersRelatedByUserRecipient', '\UsersQuery');
    }

    /**
     * Filter the query by a related \Users object
     *
     * @param \Users|ObjectCollection $users The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildUserToPremisesQuery The current query, for fluid interface
     */
    public function filterByUsersRelatedByUserSender($users, $comparison = null)
    {
        if ($users instanceof \Users) {
            return $this
                ->addUsingAlias(UserToPremisesTableMap::COL_USER_SENDER, $users->getId(), $comparison);
        } elseif ($users instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(UserToPremisesTableMap::COL_USER_SENDER, $users->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByUsersRelatedByUserSender() only accepts arguments of type \Users or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the UsersRelatedByUserSender relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildUserToPremisesQuery The current query, for fluid interface
     */
    public function joinUsersRelatedByUserSender($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('UsersRelatedByUserSender');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'UsersRelatedByUserSender');
        }

        return $this;
    }

    /**
     * Use the UsersRelatedByUserSender relation Users object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \UsersQuery A secondary query class using the current class as primary query
     */
    public function useUsersRelatedByUserSenderQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinUsersRelatedByUserSender($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'UsersRelatedByUserSender', '\UsersQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   ChildUserToPremises $userToPremises Object to remove from the list of results
     *
     * @return $this|ChildUserToPremisesQuery The current query, for fluid interface
     */
    public function prune($userToPremises = null)
    {
        if ($userToPremises) {
            $this->addUsingAlias(UserToPremisesTableMap::COL_ID, $userToPremises->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the user_to_premises table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(UserToPremisesTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            UserToPremisesTableMap::clearInstancePool();
            UserToPremisesTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(UserToPremisesTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(UserToPremisesTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            UserToPremisesTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            UserToPremisesTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

} // UserToPremisesQuery
