<?php

namespace Base;

use \Premises as ChildPremises;
use \PremisesQuery as ChildPremisesQuery;
use \Exception;
use \PDO;
use Map\PremisesTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveQuery\ModelJoin;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'premises' table.
 *
 *
 *
 * @method     ChildPremisesQuery orderById($order = Criteria::ASC) Order by the id column
 * @method     ChildPremisesQuery orderByDateSender($order = Criteria::ASC) Order by the date_sender column
 * @method     ChildPremisesQuery orderByDateRecipient($order = Criteria::ASC) Order by the date_recipient column
 * @method     ChildPremisesQuery orderByTypeId($order = Criteria::ASC) Order by the type_id column
 * @method     ChildPremisesQuery orderByCount($order = Criteria::ASC) Order by the count column
 * @method     ChildPremisesQuery orderByRouteId($order = Criteria::ASC) Order by the route_id column
 * @method     ChildPremisesQuery orderByCost($order = Criteria::ASC) Order by the cost column
 * @method     ChildPremisesQuery orderByCitySender($order = Criteria::ASC) Order by the city_sender column
 * @method     ChildPremisesQuery orderByCityRecipient($order = Criteria::ASC) Order by the city_recipient column
 * @method     ChildPremisesQuery orderByStatusId($order = Criteria::ASC) Order by the status_id column
 * @method     ChildPremisesQuery orderByPayerId($order = Criteria::ASC) Order by the payer_id column
 * @method     ChildPremisesQuery orderByComment($order = Criteria::ASC) Order by the comment column
 *
 * @method     ChildPremisesQuery groupById() Group by the id column
 * @method     ChildPremisesQuery groupByDateSender() Group by the date_sender column
 * @method     ChildPremisesQuery groupByDateRecipient() Group by the date_recipient column
 * @method     ChildPremisesQuery groupByTypeId() Group by the type_id column
 * @method     ChildPremisesQuery groupByCount() Group by the count column
 * @method     ChildPremisesQuery groupByRouteId() Group by the route_id column
 * @method     ChildPremisesQuery groupByCost() Group by the cost column
 * @method     ChildPremisesQuery groupByCitySender() Group by the city_sender column
 * @method     ChildPremisesQuery groupByCityRecipient() Group by the city_recipient column
 * @method     ChildPremisesQuery groupByStatusId() Group by the status_id column
 * @method     ChildPremisesQuery groupByPayerId() Group by the payer_id column
 * @method     ChildPremisesQuery groupByComment() Group by the comment column
 *
 * @method     ChildPremisesQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildPremisesQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildPremisesQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildPremisesQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildPremisesQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildPremisesQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildPremisesQuery leftJoinUsers($relationAlias = null) Adds a LEFT JOIN clause to the query using the Users relation
 * @method     ChildPremisesQuery rightJoinUsers($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Users relation
 * @method     ChildPremisesQuery innerJoinUsers($relationAlias = null) Adds a INNER JOIN clause to the query using the Users relation
 *
 * @method     ChildPremisesQuery joinWithUsers($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Users relation
 *
 * @method     ChildPremisesQuery leftJoinWithUsers() Adds a LEFT JOIN clause and with to the query using the Users relation
 * @method     ChildPremisesQuery rightJoinWithUsers() Adds a RIGHT JOIN clause and with to the query using the Users relation
 * @method     ChildPremisesQuery innerJoinWithUsers() Adds a INNER JOIN clause and with to the query using the Users relation
 *
 * @method     ChildPremisesQuery leftJoinRoutes($relationAlias = null) Adds a LEFT JOIN clause to the query using the Routes relation
 * @method     ChildPremisesQuery rightJoinRoutes($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Routes relation
 * @method     ChildPremisesQuery innerJoinRoutes($relationAlias = null) Adds a INNER JOIN clause to the query using the Routes relation
 *
 * @method     ChildPremisesQuery joinWithRoutes($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Routes relation
 *
 * @method     ChildPremisesQuery leftJoinWithRoutes() Adds a LEFT JOIN clause and with to the query using the Routes relation
 * @method     ChildPremisesQuery rightJoinWithRoutes() Adds a RIGHT JOIN clause and with to the query using the Routes relation
 * @method     ChildPremisesQuery innerJoinWithRoutes() Adds a INNER JOIN clause and with to the query using the Routes relation
 *
 * @method     ChildPremisesQuery leftJoinStatuses($relationAlias = null) Adds a LEFT JOIN clause to the query using the Statuses relation
 * @method     ChildPremisesQuery rightJoinStatuses($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Statuses relation
 * @method     ChildPremisesQuery innerJoinStatuses($relationAlias = null) Adds a INNER JOIN clause to the query using the Statuses relation
 *
 * @method     ChildPremisesQuery joinWithStatuses($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Statuses relation
 *
 * @method     ChildPremisesQuery leftJoinWithStatuses() Adds a LEFT JOIN clause and with to the query using the Statuses relation
 * @method     ChildPremisesQuery rightJoinWithStatuses() Adds a RIGHT JOIN clause and with to the query using the Statuses relation
 * @method     ChildPremisesQuery innerJoinWithStatuses() Adds a INNER JOIN clause and with to the query using the Statuses relation
 *
 * @method     ChildPremisesQuery leftJoinTypesPremise($relationAlias = null) Adds a LEFT JOIN clause to the query using the TypesPremise relation
 * @method     ChildPremisesQuery rightJoinTypesPremise($relationAlias = null) Adds a RIGHT JOIN clause to the query using the TypesPremise relation
 * @method     ChildPremisesQuery innerJoinTypesPremise($relationAlias = null) Adds a INNER JOIN clause to the query using the TypesPremise relation
 *
 * @method     ChildPremisesQuery joinWithTypesPremise($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the TypesPremise relation
 *
 * @method     ChildPremisesQuery leftJoinWithTypesPremise() Adds a LEFT JOIN clause and with to the query using the TypesPremise relation
 * @method     ChildPremisesQuery rightJoinWithTypesPremise() Adds a RIGHT JOIN clause and with to the query using the TypesPremise relation
 * @method     ChildPremisesQuery innerJoinWithTypesPremise() Adds a INNER JOIN clause and with to the query using the TypesPremise relation
 *
 * @method     ChildPremisesQuery leftJoinUserToPremises($relationAlias = null) Adds a LEFT JOIN clause to the query using the UserToPremises relation
 * @method     ChildPremisesQuery rightJoinUserToPremises($relationAlias = null) Adds a RIGHT JOIN clause to the query using the UserToPremises relation
 * @method     ChildPremisesQuery innerJoinUserToPremises($relationAlias = null) Adds a INNER JOIN clause to the query using the UserToPremises relation
 *
 * @method     ChildPremisesQuery joinWithUserToPremises($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the UserToPremises relation
 *
 * @method     ChildPremisesQuery leftJoinWithUserToPremises() Adds a LEFT JOIN clause and with to the query using the UserToPremises relation
 * @method     ChildPremisesQuery rightJoinWithUserToPremises() Adds a RIGHT JOIN clause and with to the query using the UserToPremises relation
 * @method     ChildPremisesQuery innerJoinWithUserToPremises() Adds a INNER JOIN clause and with to the query using the UserToPremises relation
 *
 * @method     \UsersQuery|\RoutesQuery|\StatusesQuery|\TypesPremiseQuery|\UserToPremisesQuery endUse() Finalizes a secondary criteria and merges it with its primary Criteria
 *
 * @method     ChildPremises findOne(ConnectionInterface $con = null) Return the first ChildPremises matching the query
 * @method     ChildPremises findOneOrCreate(ConnectionInterface $con = null) Return the first ChildPremises matching the query, or a new ChildPremises object populated from the query conditions when no match is found
 *
 * @method     ChildPremises findOneById(string $id) Return the first ChildPremises filtered by the id column
 * @method     ChildPremises findOneByDateSender(string $date_sender) Return the first ChildPremises filtered by the date_sender column
 * @method     ChildPremises findOneByDateRecipient(int $date_recipient) Return the first ChildPremises filtered by the date_recipient column
 * @method     ChildPremises findOneByTypeId(int $type_id) Return the first ChildPremises filtered by the type_id column
 * @method     ChildPremises findOneByCount(int $count) Return the first ChildPremises filtered by the count column
 * @method     ChildPremises findOneByRouteId(string $route_id) Return the first ChildPremises filtered by the route_id column
 * @method     ChildPremises findOneByCost(int $cost) Return the first ChildPremises filtered by the cost column
 * @method     ChildPremises findOneByCitySender(string $city_sender) Return the first ChildPremises filtered by the city_sender column
 * @method     ChildPremises findOneByCityRecipient(string $city_recipient) Return the first ChildPremises filtered by the city_recipient column
 * @method     ChildPremises findOneByStatusId(int $status_id) Return the first ChildPremises filtered by the status_id column
 * @method     ChildPremises findOneByPayerId(string $payer_id) Return the first ChildPremises filtered by the payer_id column
 * @method     ChildPremises findOneByComment(string $comment) Return the first ChildPremises filtered by the comment column *

 * @method     ChildPremises requirePk($key, ConnectionInterface $con = null) Return the ChildPremises by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPremises requireOne(ConnectionInterface $con = null) Return the first ChildPremises matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildPremises requireOneById(string $id) Return the first ChildPremises filtered by the id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPremises requireOneByDateSender(string $date_sender) Return the first ChildPremises filtered by the date_sender column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPremises requireOneByDateRecipient(int $date_recipient) Return the first ChildPremises filtered by the date_recipient column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPremises requireOneByTypeId(int $type_id) Return the first ChildPremises filtered by the type_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPremises requireOneByCount(int $count) Return the first ChildPremises filtered by the count column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPremises requireOneByRouteId(string $route_id) Return the first ChildPremises filtered by the route_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPremises requireOneByCost(int $cost) Return the first ChildPremises filtered by the cost column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPremises requireOneByCitySender(string $city_sender) Return the first ChildPremises filtered by the city_sender column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPremises requireOneByCityRecipient(string $city_recipient) Return the first ChildPremises filtered by the city_recipient column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPremises requireOneByStatusId(int $status_id) Return the first ChildPremises filtered by the status_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPremises requireOneByPayerId(string $payer_id) Return the first ChildPremises filtered by the payer_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPremises requireOneByComment(string $comment) Return the first ChildPremises filtered by the comment column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildPremises[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildPremises objects based on current ModelCriteria
 * @method     ChildPremises[]|ObjectCollection findById(string $id) Return ChildPremises objects filtered by the id column
 * @method     ChildPremises[]|ObjectCollection findByDateSender(string $date_sender) Return ChildPremises objects filtered by the date_sender column
 * @method     ChildPremises[]|ObjectCollection findByDateRecipient(int $date_recipient) Return ChildPremises objects filtered by the date_recipient column
 * @method     ChildPremises[]|ObjectCollection findByTypeId(int $type_id) Return ChildPremises objects filtered by the type_id column
 * @method     ChildPremises[]|ObjectCollection findByCount(int $count) Return ChildPremises objects filtered by the count column
 * @method     ChildPremises[]|ObjectCollection findByRouteId(string $route_id) Return ChildPremises objects filtered by the route_id column
 * @method     ChildPremises[]|ObjectCollection findByCost(int $cost) Return ChildPremises objects filtered by the cost column
 * @method     ChildPremises[]|ObjectCollection findByCitySender(string $city_sender) Return ChildPremises objects filtered by the city_sender column
 * @method     ChildPremises[]|ObjectCollection findByCityRecipient(string $city_recipient) Return ChildPremises objects filtered by the city_recipient column
 * @method     ChildPremises[]|ObjectCollection findByStatusId(int $status_id) Return ChildPremises objects filtered by the status_id column
 * @method     ChildPremises[]|ObjectCollection findByPayerId(string $payer_id) Return ChildPremises objects filtered by the payer_id column
 * @method     ChildPremises[]|ObjectCollection findByComment(string $comment) Return ChildPremises objects filtered by the comment column
 * @method     ChildPremises[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class PremisesQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \Base\PremisesQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'crm', $modelName = '\\Premises', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildPremisesQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildPremisesQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildPremisesQuery) {
            return $criteria;
        }
        $query = new ChildPremisesQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildPremises|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(PremisesTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = PremisesTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildPremises A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT id, date_sender, date_recipient, type_id, count, route_id, cost, city_sender, city_recipient, status_id, payer_id, comment FROM premises WHERE id = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildPremises $obj */
            $obj = new ChildPremises();
            $obj->hydrate($row);
            PremisesTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildPremises|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildPremisesQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(PremisesTableMap::COL_ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildPremisesQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(PremisesTableMap::COL_ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE id = 1234
     * $query->filterById(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE id > 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPremisesQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(PremisesTableMap::COL_ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(PremisesTableMap::COL_ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PremisesTableMap::COL_ID, $id, $comparison);
    }

    /**
     * Filter the query on the date_sender column
     *
     * Example usage:
     * <code>
     * $query->filterByDateSender(1234); // WHERE date_sender = 1234
     * $query->filterByDateSender(array(12, 34)); // WHERE date_sender IN (12, 34)
     * $query->filterByDateSender(array('min' => 12)); // WHERE date_sender > 12
     * </code>
     *
     * @param     mixed $dateSender The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPremisesQuery The current query, for fluid interface
     */
    public function filterByDateSender($dateSender = null, $comparison = null)
    {
        if (is_array($dateSender)) {
            $useMinMax = false;
            if (isset($dateSender['min'])) {
                $this->addUsingAlias(PremisesTableMap::COL_DATE_SENDER, $dateSender['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($dateSender['max'])) {
                $this->addUsingAlias(PremisesTableMap::COL_DATE_SENDER, $dateSender['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PremisesTableMap::COL_DATE_SENDER, $dateSender, $comparison);
    }

    /**
     * Filter the query on the date_recipient column
     *
     * Example usage:
     * <code>
     * $query->filterByDateRecipient(1234); // WHERE date_recipient = 1234
     * $query->filterByDateRecipient(array(12, 34)); // WHERE date_recipient IN (12, 34)
     * $query->filterByDateRecipient(array('min' => 12)); // WHERE date_recipient > 12
     * </code>
     *
     * @param     mixed $dateRecipient The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPremisesQuery The current query, for fluid interface
     */
    public function filterByDateRecipient($dateRecipient = null, $comparison = null)
    {
        if (is_array($dateRecipient)) {
            $useMinMax = false;
            if (isset($dateRecipient['min'])) {
                $this->addUsingAlias(PremisesTableMap::COL_DATE_RECIPIENT, $dateRecipient['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($dateRecipient['max'])) {
                $this->addUsingAlias(PremisesTableMap::COL_DATE_RECIPIENT, $dateRecipient['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PremisesTableMap::COL_DATE_RECIPIENT, $dateRecipient, $comparison);
    }

    /**
     * Filter the query on the type_id column
     *
     * Example usage:
     * <code>
     * $query->filterByTypeId(1234); // WHERE type_id = 1234
     * $query->filterByTypeId(array(12, 34)); // WHERE type_id IN (12, 34)
     * $query->filterByTypeId(array('min' => 12)); // WHERE type_id > 12
     * </code>
     *
     * @see       filterByTypesPremise()
     *
     * @param     mixed $typeId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPremisesQuery The current query, for fluid interface
     */
    public function filterByTypeId($typeId = null, $comparison = null)
    {
        if (is_array($typeId)) {
            $useMinMax = false;
            if (isset($typeId['min'])) {
                $this->addUsingAlias(PremisesTableMap::COL_TYPE_ID, $typeId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($typeId['max'])) {
                $this->addUsingAlias(PremisesTableMap::COL_TYPE_ID, $typeId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PremisesTableMap::COL_TYPE_ID, $typeId, $comparison);
    }

    /**
     * Filter the query on the count column
     *
     * Example usage:
     * <code>
     * $query->filterByCount(1234); // WHERE count = 1234
     * $query->filterByCount(array(12, 34)); // WHERE count IN (12, 34)
     * $query->filterByCount(array('min' => 12)); // WHERE count > 12
     * </code>
     *
     * @param     mixed $count The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPremisesQuery The current query, for fluid interface
     */
    public function filterByCount($count = null, $comparison = null)
    {
        if (is_array($count)) {
            $useMinMax = false;
            if (isset($count['min'])) {
                $this->addUsingAlias(PremisesTableMap::COL_COUNT, $count['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($count['max'])) {
                $this->addUsingAlias(PremisesTableMap::COL_COUNT, $count['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PremisesTableMap::COL_COUNT, $count, $comparison);
    }

    /**
     * Filter the query on the route_id column
     *
     * Example usage:
     * <code>
     * $query->filterByRouteId(1234); // WHERE route_id = 1234
     * $query->filterByRouteId(array(12, 34)); // WHERE route_id IN (12, 34)
     * $query->filterByRouteId(array('min' => 12)); // WHERE route_id > 12
     * </code>
     *
     * @see       filterByRoutes()
     *
     * @param     mixed $routeId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPremisesQuery The current query, for fluid interface
     */
    public function filterByRouteId($routeId = null, $comparison = null)
    {
        if (is_array($routeId)) {
            $useMinMax = false;
            if (isset($routeId['min'])) {
                $this->addUsingAlias(PremisesTableMap::COL_ROUTE_ID, $routeId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($routeId['max'])) {
                $this->addUsingAlias(PremisesTableMap::COL_ROUTE_ID, $routeId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PremisesTableMap::COL_ROUTE_ID, $routeId, $comparison);
    }

    /**
     * Filter the query on the cost column
     *
     * Example usage:
     * <code>
     * $query->filterByCost(1234); // WHERE cost = 1234
     * $query->filterByCost(array(12, 34)); // WHERE cost IN (12, 34)
     * $query->filterByCost(array('min' => 12)); // WHERE cost > 12
     * </code>
     *
     * @param     mixed $cost The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPremisesQuery The current query, for fluid interface
     */
    public function filterByCost($cost = null, $comparison = null)
    {
        if (is_array($cost)) {
            $useMinMax = false;
            if (isset($cost['min'])) {
                $this->addUsingAlias(PremisesTableMap::COL_COST, $cost['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($cost['max'])) {
                $this->addUsingAlias(PremisesTableMap::COL_COST, $cost['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PremisesTableMap::COL_COST, $cost, $comparison);
    }

    /**
     * Filter the query on the city_sender column
     *
     * Example usage:
     * <code>
     * $query->filterByCitySender('fooValue');   // WHERE city_sender = 'fooValue'
     * $query->filterByCitySender('%fooValue%', Criteria::LIKE); // WHERE city_sender LIKE '%fooValue%'
     * </code>
     *
     * @param     string $citySender The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPremisesQuery The current query, for fluid interface
     */
    public function filterByCitySender($citySender = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($citySender)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PremisesTableMap::COL_CITY_SENDER, $citySender, $comparison);
    }

    /**
     * Filter the query on the city_recipient column
     *
     * Example usage:
     * <code>
     * $query->filterByCityRecipient('fooValue');   // WHERE city_recipient = 'fooValue'
     * $query->filterByCityRecipient('%fooValue%', Criteria::LIKE); // WHERE city_recipient LIKE '%fooValue%'
     * </code>
     *
     * @param     string $cityRecipient The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPremisesQuery The current query, for fluid interface
     */
    public function filterByCityRecipient($cityRecipient = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($cityRecipient)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PremisesTableMap::COL_CITY_RECIPIENT, $cityRecipient, $comparison);
    }

    /**
     * Filter the query on the status_id column
     *
     * Example usage:
     * <code>
     * $query->filterByStatusId(1234); // WHERE status_id = 1234
     * $query->filterByStatusId(array(12, 34)); // WHERE status_id IN (12, 34)
     * $query->filterByStatusId(array('min' => 12)); // WHERE status_id > 12
     * </code>
     *
     * @see       filterByStatuses()
     *
     * @param     mixed $statusId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPremisesQuery The current query, for fluid interface
     */
    public function filterByStatusId($statusId = null, $comparison = null)
    {
        if (is_array($statusId)) {
            $useMinMax = false;
            if (isset($statusId['min'])) {
                $this->addUsingAlias(PremisesTableMap::COL_STATUS_ID, $statusId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($statusId['max'])) {
                $this->addUsingAlias(PremisesTableMap::COL_STATUS_ID, $statusId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PremisesTableMap::COL_STATUS_ID, $statusId, $comparison);
    }

    /**
     * Filter the query on the payer_id column
     *
     * Example usage:
     * <code>
     * $query->filterByPayerId(1234); // WHERE payer_id = 1234
     * $query->filterByPayerId(array(12, 34)); // WHERE payer_id IN (12, 34)
     * $query->filterByPayerId(array('min' => 12)); // WHERE payer_id > 12
     * </code>
     *
     * @see       filterByUsers()
     *
     * @param     mixed $payerId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPremisesQuery The current query, for fluid interface
     */
    public function filterByPayerId($payerId = null, $comparison = null)
    {
        if (is_array($payerId)) {
            $useMinMax = false;
            if (isset($payerId['min'])) {
                $this->addUsingAlias(PremisesTableMap::COL_PAYER_ID, $payerId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($payerId['max'])) {
                $this->addUsingAlias(PremisesTableMap::COL_PAYER_ID, $payerId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PremisesTableMap::COL_PAYER_ID, $payerId, $comparison);
    }

    /**
     * Filter the query on the comment column
     *
     * Example usage:
     * <code>
     * $query->filterByComment('fooValue');   // WHERE comment = 'fooValue'
     * $query->filterByComment('%fooValue%', Criteria::LIKE); // WHERE comment LIKE '%fooValue%'
     * </code>
     *
     * @param     string $comment The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPremisesQuery The current query, for fluid interface
     */
    public function filterByComment($comment = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($comment)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PremisesTableMap::COL_COMMENT, $comment, $comparison);
    }

    /**
     * Filter the query by a related \Users object
     *
     * @param \Users|ObjectCollection $users The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildPremisesQuery The current query, for fluid interface
     */
    public function filterByUsers($users, $comparison = null)
    {
        if ($users instanceof \Users) {
            return $this
                ->addUsingAlias(PremisesTableMap::COL_PAYER_ID, $users->getId(), $comparison);
        } elseif ($users instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(PremisesTableMap::COL_PAYER_ID, $users->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByUsers() only accepts arguments of type \Users or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Users relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildPremisesQuery The current query, for fluid interface
     */
    public function joinUsers($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Users');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Users');
        }

        return $this;
    }

    /**
     * Use the Users relation Users object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \UsersQuery A secondary query class using the current class as primary query
     */
    public function useUsersQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinUsers($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Users', '\UsersQuery');
    }

    /**
     * Filter the query by a related \Routes object
     *
     * @param \Routes|ObjectCollection $routes The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildPremisesQuery The current query, for fluid interface
     */
    public function filterByRoutes($routes, $comparison = null)
    {
        if ($routes instanceof \Routes) {
            return $this
                ->addUsingAlias(PremisesTableMap::COL_ROUTE_ID, $routes->getId(), $comparison);
        } elseif ($routes instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(PremisesTableMap::COL_ROUTE_ID, $routes->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByRoutes() only accepts arguments of type \Routes or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Routes relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildPremisesQuery The current query, for fluid interface
     */
    public function joinRoutes($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Routes');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Routes');
        }

        return $this;
    }

    /**
     * Use the Routes relation Routes object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \RoutesQuery A secondary query class using the current class as primary query
     */
    public function useRoutesQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinRoutes($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Routes', '\RoutesQuery');
    }

    /**
     * Filter the query by a related \Statuses object
     *
     * @param \Statuses|ObjectCollection $statuses The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildPremisesQuery The current query, for fluid interface
     */
    public function filterByStatuses($statuses, $comparison = null)
    {
        if ($statuses instanceof \Statuses) {
            return $this
                ->addUsingAlias(PremisesTableMap::COL_STATUS_ID, $statuses->getId(), $comparison);
        } elseif ($statuses instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(PremisesTableMap::COL_STATUS_ID, $statuses->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByStatuses() only accepts arguments of type \Statuses or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Statuses relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildPremisesQuery The current query, for fluid interface
     */
    public function joinStatuses($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Statuses');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Statuses');
        }

        return $this;
    }

    /**
     * Use the Statuses relation Statuses object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \StatusesQuery A secondary query class using the current class as primary query
     */
    public function useStatusesQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinStatuses($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Statuses', '\StatusesQuery');
    }

    /**
     * Filter the query by a related \TypesPremise object
     *
     * @param \TypesPremise|ObjectCollection $typesPremise The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildPremisesQuery The current query, for fluid interface
     */
    public function filterByTypesPremise($typesPremise, $comparison = null)
    {
        if ($typesPremise instanceof \TypesPremise) {
            return $this
                ->addUsingAlias(PremisesTableMap::COL_TYPE_ID, $typesPremise->getId(), $comparison);
        } elseif ($typesPremise instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(PremisesTableMap::COL_TYPE_ID, $typesPremise->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByTypesPremise() only accepts arguments of type \TypesPremise or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the TypesPremise relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildPremisesQuery The current query, for fluid interface
     */
    public function joinTypesPremise($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('TypesPremise');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'TypesPremise');
        }

        return $this;
    }

    /**
     * Use the TypesPremise relation TypesPremise object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \TypesPremiseQuery A secondary query class using the current class as primary query
     */
    public function useTypesPremiseQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinTypesPremise($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'TypesPremise', '\TypesPremiseQuery');
    }

    /**
     * Filter the query by a related \UserToPremises object
     *
     * @param \UserToPremises|ObjectCollection $userToPremises the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildPremisesQuery The current query, for fluid interface
     */
    public function filterByUserToPremises($userToPremises, $comparison = null)
    {
        if ($userToPremises instanceof \UserToPremises) {
            return $this
                ->addUsingAlias(PremisesTableMap::COL_ID, $userToPremises->getPremiseId(), $comparison);
        } elseif ($userToPremises instanceof ObjectCollection) {
            return $this
                ->useUserToPremisesQuery()
                ->filterByPrimaryKeys($userToPremises->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByUserToPremises() only accepts arguments of type \UserToPremises or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the UserToPremises relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildPremisesQuery The current query, for fluid interface
     */
    public function joinUserToPremises($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('UserToPremises');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'UserToPremises');
        }

        return $this;
    }

    /**
     * Use the UserToPremises relation UserToPremises object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \UserToPremisesQuery A secondary query class using the current class as primary query
     */
    public function useUserToPremisesQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinUserToPremises($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'UserToPremises', '\UserToPremisesQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   ChildPremises $premises Object to remove from the list of results
     *
     * @return $this|ChildPremisesQuery The current query, for fluid interface
     */
    public function prune($premises = null)
    {
        if ($premises) {
            $this->addUsingAlias(PremisesTableMap::COL_ID, $premises->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the premises table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(PremisesTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            PremisesTableMap::clearInstancePool();
            PremisesTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(PremisesTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(PremisesTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            PremisesTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            PremisesTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

} // PremisesQuery
