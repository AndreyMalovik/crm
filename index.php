<?php

error_reporting(E_ALL);
/* Подключение и инициализация Ррутинга */
require_once __DIR__ . '/vendor/autoload.php';
require_once __DIR__ . '/config/config.php';
$klein = new \Klein\Klein();

$klein->respond('GET', '/premises', function ($request, $response) {

    if (!empty($_GET)) {
        $con = Propel\Runtime\Propel::getWriteConnection(Map\UsersTableMap::DATABASE_NAME);
        $premises = [];
        $filterName = '';
        $filterRoute = '';
        $filterComment = '';
        $filterStatus = '';
        $filterDate = '';
        $and = '';
        $or = '';
        $countParams = count($_GET);

        if (!empty($_GET['name'])) {
            //$name = 'user2';
            $name = strip_tags($_GET['name']);
            $name = htmlspecialchars($_GET['name']);
            $filterName = " AND username LIKE '" . $name . "'";
        }
        if (!empty($_GET['route'])) {
            //$name = 'user2';
            $route = strip_tags($_GET['route']);
            $route = htmlspecialchars($_GET['route']);
            $filterRoute = " AND routes.name LIKE '" . $route . "'";
        }

        if (!empty($_GET['comment'])) {
            //$name = 'user2';
            $comment = strip_tags($_GET['comment']);
            $comment = htmlspecialchars($_GET['comment']);
            $filterComment = " AND premises.comment LIKE '" . $comment . "'";
        }

        if (!empty($_GET['status'])) {
            //$name = 'user2';
            $status = strip_tags($_GET['status']);
            $status = htmlspecialchars($_GET['status']);
            $filterStatus = " AND statuses.name LIKE '" . $status . "'";
        }

        if (!empty($_GET['date'])) {
            //$name = 'user2';
            $date = strtotime($_GET['date']);
            //1531602000
            // echo date('d.m.Y',$date);
            $filterDate = " AND premises.date_sender = '" . $date . "' OR premises.date_recipient = '" . $date . "'";
        }

        if ($countParams <= 1) {
            $and = '';
        } else {
            $and = ' AND ';
        }
        $sql = "SELECT user_sender,premise_id,user_recipient,trust_user_id,route_id FROM `users` LEFT JOIN user_to_premises ON (user_to_premises.user_sender=users.id) LEFT JOIN premises ON(user_to_premises.premise_id = premises.id) LEFT JOIN routes ON(premises.route_id = routes.id) LEFT JOIN statuses ON(premises.status_id = statuses.id) WHERE 1" . $filterName . $filterRoute . $filterComment . $filterStatus . $filterDate;

        $str = str_word_count($sql, 1);
        if (trim(array_pop($str)) === 'AND') {
            $sql = preg_replace('/\W\w+\s*(\W*)$/', '$1', $sql);
        }
        // echo $countParams;
        //echo $and;
        echo $sql;
        $res = $con->query($sql);

        while ($row = $res->fetch()) {
            print_r($row);
            $premises[] = [
                'sender_id' => $row[0],
                'premise_id' => $row[1],
                'recipient_id' => $row[2],
                'trust_id' => $row[3],
                    // 'payer_id' => $row[4],
            ];
        }
        return json_encode($premises);
    } else {
        $premises = PremisesQuery::create()->find();
        /* echo '<pre>';
          print_r($premises->toArray());
          echo '</pre>'; */
        return $premises->toJSON();
    }

    //SELECT user_sender,premise_id,user_recipient,trust_user_id FROM `users` LEFT JOIN user_to_premises ON (user_to_premises.user_sender=users.id) WHERE username LIKE 'user1' 
//SELECT user_sender,premise_id,user_recipient,trust_user_id FROM `users` LEFT JOIN user_to_premises ON (user_to_premises.user_sender=users.id) LEFT JOIN premises ON(user_to_premises.premise_id = premises.id) LEFT JOIN routes ON(premises.route_id = routes.id) WHERE username LIKE 'user2' AND routes.name LIKE 'Черновцы-Одесса'
});
/* Платежи по посылкам */
$klein->respond('GET', '/payments', function ($request, $response) {
    $payments = PaymentsQuery::create()->find();
    return $payments->toJSON();
});

$klein->respond('GET', '/user/[:id]/payments', function ($request, $response) {
    $con = Propel\Runtime\Propel::getWriteConnection(Map\UsersTableMap::DATABASE_NAME);
    $Id = intval($request->id);
    $payments = [];
    $sql = "SELECT * FROM `payments` WHERE user_id = " . $Id;
    echo $sql;
    $res = $con->query($sql);

    while ($row = $res->fetch()) {
        print_r($row);
        $payments[] = [
            'id' => $row[0],
            'user_id' => $row[1],
            'amount' => $row[2],
        ];
    }
    return json_encode($payments);
});

/* Типы посылок */
$klein->respond('GET', '/premises/types', function ($request, $response) {
    $premisesTypes = TypesPremiseQuery::create()->find();
    /* echo '<pre>';
      print_r($premisesTypes->toArray());
      echo '</pre>'; */
    return $premisesTypes->toJSON();
});

$klein->respond('POST', '/premises/type', function ($request, $response) {
    $premiseType = new TypesPremise();
    //$premiseTypeName = 'бандероль';

    $premiseTypeName = strip_tags($request->typeName);
    $premiseTypeName = htmlspecialchars($request->typeName);

    $premiseType->setName($premiseTypeName);
    $premiseType->save();
    return $premiseType->getId();
});

$klein->respond('DELETE', '/premises/type/[:id]', function ($request, $response) {
    $Id = intval($request->id);
    $premiseTypeId = TypesPremiseQuery::create()->findPk($Id);
    if ($premiseTypeId->delete()) {
        return 'Невозможно удалить тип,возможно в БД эсть посылки этого типа';
    } else {
        return 'Успешно удален';
    }
});

$klein->respond('PATCH', '/premises/type/[:id]', function ($request, $response) {
    $Id = intval($request->id);
    $premiseType = TypesPremiseQuery::create()->findPk($Id);

    // $premiseTypeName = 'бандероль';
    $premiseTypeName = strip_tags($request->typeName);
    $premiseTypeName = htmlspecialchars($request->typeName);

    $premiseType->setName($premiseTypeName);
    $premiseType->save();

    return 'Тип успешно изменен';
});
/* ------------------------------------------------------------------ */


/* Статусы посылок */
$klein->respond('GET', '/premises/statuses', function ($request, $response) {
    $premisesStatuses = StatusesQuery::create()->find();
    /* echo '<pre>';
      print_r($premisesStatuses->toArray());
      echo '</pre>'; */
    return $premisesStatuses->toJSON();
});

$klein->respond('POST', '/premises/statuse', function ($request, $response) {
    $statuses = new Statuses();
    //$statuseName = 'testttt statuse';
    $statuseName = strip_tags($request->statuseName);
    $statuseName = htmlspecialchars($request->statuseName);

    $statuses->setName($statuseName);
    $statuses->save();
    return $statuses->getId();
});

$klein->respond('DELETE', '/premises/statuse/[:id]', function ($request, $response) {
    $Id = intval($request->id);
    $statuse = StatusesQuery::create()->findPk($Id);
    $statuse->delete();
    return 'Статус Успешно удален';
});

$klein->respond('PATCH', '/premises/statuse/[:id]', function ($request, $response) {
    $Id = intval($request->id);
    $statuse = StatusesQuery::create()->findPk($Id);

    $statuseName = 'тестовый статус';
    /* $statuseName = strip_tags($request->statuseName);
      $statuseName = htmlspecialchars($request->statuseName);
      $statuseName = mysql_escape_string($request->statuseName); */

    $statuse->setName($statuseName);
    $statuse->save();

    return 'Статус успешно изменен';
});
/* ------------------------------------------------------------------ */


/* Маршруты посылок */
$klein->respond('GET', '/premises/routes', function ($request, $response) {
    $premisesRoutes = RoutesQuery::create()->find();
    echo '<pre>';
    print_r($premisesRoutes->toArray());
    echo '</pre>';
    return $premisesRoutes->toJSON();
});

$klein->respond('POST', '/premises/route', function ($request, $response) {
    $route = new Routes();
    //$premiseTypeName = 'бандероль';
    //$routeName = 'test route';
    $routeName = strip_tags($request->routeName);
    $routeName = htmlspecialchars($request->routeName);

    $route->setName($routeName);
    $route->save();
    return $route->getId();
});

$klein->respond('DELETE', '/premises/route/[:id]', function ($request, $response) {
    $Id = intval($request->id);
    $route = RoutesQuery::create()->findPk($Id);
    $route->delete();
    return 'Маршрут Успешно удален';
});

$klein->respond('PATCH', '/premises/route/[:id]', function ($request, $response) {
    $Id = intval($request->id);
    $route = RoutesQuery::create()->findPk($Id);

    //$routeName = 'Одесса-Днепр';
    $routeName = strip_tags($request->routeName);
    $routeName = htmlspecialchars($request->routeName);

    $route->setName($routeName);
    $route->save();

    return 'Статус успешно изменен';
});
/* ------------------------------------------------------------------ */


/* Посылки */
$klein->respond('POST', '/premise', function ($request, $response) {

    /* Тестовые Данные для заполнения */
    /* $dateSender = '15.07.2018';
      $dateRecipient = '20.07.2018';
      $dateSender = strtotime($dateSender); //54564654;
      $dateRecipient = strtotime($dateRecipient); //54564645;
      $typeId = 2;
      $count = 4;
      $routeId = 3;
      $cost = 38;
      $citySender = 'Одесса';
      $cityRecipient = 'Кривой Рог';
      $statusId = 2;
      $comment = 'gigugigu';
      $payer = 'sender';
      $userSenderUsername = 'user1'; */

    /* ----------------------------------------------------- */

    /* Данные Посылки */
    $dateSender = intval($request->dateSender);
    $dateRecipient = intval($request->dateRecipient);
    $typeId = intval($request->typeId);
    $count = intval($request->count);
    $routeId = intval($request->routeId);
    $cost = intval($request->cost);

    $city_sender = strip_tags($request->city_sender);
    $city_sender = htmlspecialchars($request->city_sender);

    $city_recipient = strip_tags($request->city_recipient);
    $city_recipient = htmlspecialchars($request->city_recipient);

    $statusId = intval($request->statusId);

    $comment = strip_tags($request->comment);
    $comment = htmlspecialchars($request->comment);

    /* echo '<pre>';
      print_r($premises);
      echo '</pre>'; */

    $payer = intval($request->payer); // узнаем кто оплачивает(отправитель или получатель)
    if ($payer === 'sender') {
        $userId = Users::checkUser($userSenderUsername); //проверяю существует ли пользователь
        //с таким именем в базе
        //(может он уже ранее отправлял посылки).И если есть юзер с таким именем получаю его данные
        //сохраняю посылку в БД и возврыщаю ID юзера (payer_id)
        // echo $userId;
        if ($userId != null && $userId != 0) {
            $premise = new Premises();
            $premise->setDateSender($dateSender);
            $premise->setDateRecipient($dateRecipient);
            $premise->setTypeId($typeId);
            $premise->setCount($count);
            $premise->setRouteId($routeId);
            $premise->setCost($cost);
            $premise->setCitySender($citySender);
            $premise->setCityRecipient($cityRecipient);
            $premise->setStatusId($statusId);
            $premise->setPayerId($userId);
            $premise->setComment($comment);
            if ($premise->save()) //сохраняю посылку
                return $premise->getId(); //возвращаю ID сохраненной посылки
        } else {
            return 'Пользователя нет в БД';
        }
    }

    if ($payer === 'recipient') {
        $userId = Users::checkUser($userSenderUsername); //проверяю существует ли пользователь
        //с таким именем в базе
        //(может он уже ранее отправлял посылки).И если есть юзер с таким именем получаю его данные
        //сохраняю посылку в БД и возврыщаю ID юзера (payer_id)
        // echo $userId;
        if ($userId != null && $userId != 0) {
            $premise = new Premises();
            $premise->setDateSender($dateSender);
            $premise->setDateRecipient($dateRecipient);
            $premise->setTypeId($typeId);
            $premise->setCount($count);
            $premise->setRouteId($routeId);
            $premise->setCost($cost);
            $premise->setCitySender($citySender);
            $premise->setCityRecipient($cityRecipient);
            $premise->setStatusId($statusId);
            $premise->setPayerId($userId);
            $premise->setComment($comment);
            if ($premise->save()) //сохраняю посылку
                return $premise->getId(); //возвращаю ID сохраненной посылки
        } else {
            return 'Пользователя нет в БД';
        }
    }
});



$klein->respond('GET', '/premise/[:id]', function ($request, $response) {
    $premise = PremisesQuery::create()->findPk($request->id);
    /* echo '<pre>';
      print_r($premises);
      echo '</pre>'; */
    return $premise->toJSON();
});

/* Посылки Юзера */
$klein->respond('GET', '/user/[:id]/sender/premises', function ($request, $response) {
    $user_to_premises = UserToPremisesQuery::create()->joinWithPremises()->filterByUserSender($request->id)->find();
    echo '<pre>';
    print_r($user_to_premises->toArray());
    echo '</pre>';
    if (!empty($user_to_premises)) {
        return $user_to_premises->toJSON();
    } else {
        return 'Посылок не обнаружено';
    }
});

$klein->respond('GET', '/user/[:id]/recipient/premises', function ($request, $response) {
    $user_to_premises = UserToPremisesQuery::create()->joinWithPremises()->filterByUserRecipient($request->id)->find();
    echo '<pre>';
    print_r($user_to_premises->toArray());
    echo '</pre>';
    if (!empty($user_to_premises)) {
        return $user_to_premises->toJSON();
    } else {
        return 'Посылок не обнаружено';
    }
});

/* Юзеры */
$klein->respond('GET', '/users', function ($request, $response) {
    $users = UsersQuery::create()->find();
    /* echo '<pre>';
      print_r($users->toArray());
      echo '</pre>'; */
    return $users->toJSON();
});

$klein->respond('POST', '/user', function ($request, $response) {
    $user = new Users();
    /* Тестовые данные */
    /* $username = 'user test';
      $phone = '098-244-48-24';
      $region = 'test region';
      $point_of_sale = 'test point_of_sale';
      $specialization = 'test specialization'; */
    /* ------------------------------------ */

    /* Данные Юзера */

    $username = strip_tags($request->username);
    $username = htmlspecialchars($request->username);

    $phone = strip_tags($request->phone);
    $phone = htmlspecialchars($request->phone);

    $region = strip_tags($request->region);
    $region = htmlspecialchars($request->region);

    $point_of_sale = strip_tags($request->point_of_sale);
    $point_of_sale = htmlspecialchars($request->point_of_sale);
    $point_of_sale = mysql_escape_string($request->point_of_sale);

    $specialization = strip_tags($request->specialization);
    $specialization = htmlspecialchars($request->specialization);

    $user->setUsername($username);
    $user->setPhone($phone);
    $user->setRegion($region);
    $user->setPointOfSale($point_of_sale);
    $user->setSpecialization($specialization);

    $user->save();
    return $user->getId();
});

$klein->respond('DELETE', '/user/[:id]', function ($request, $response) {
    $Id = intval($request->id);
    $user = UsersQuery::create()->findPk($Id);
    $user->delete();
    return 'Пользователь Успешно удален';
});

$klein->respond('PATCH', '/user/[:id]', function ($request, $response) {

    $Id = intval($request->id);
    $user = UsersQuery::create()->findPk($Id);
    /* Тестовые данные */
    if ($user) {
        /*  $username = 'user test';
          $phone = '098-244-48-24';
          $region = 'test region';
          $point_of_sale = 'test point_of_sale';
          $specialization = 'test specialization'; */
        /* ------------------------------------ */

        /* Данные Юзера */

        $username = strip_tags($request->username);
        $username = htmlspecialchars($request->username);

        $phone = strip_tags($request->phone);
        $phone = htmlspecialchars($request->phone);

        $region = strip_tags($request->region);
        $region = htmlspecialchars($request->region);

        $point_of_sale = strip_tags($request->point_of_sale);
        $point_of_sale = htmlspecialchars($request->point_of_sale);

        $specialization = strip_tags($request->specialization);
        $specialization = htmlspecialchars($request->specialization);

        $user->setUsername($username);
        $user->setPhone($phone);
        $user->setRegion($region);
        $user->setPointOfSale($point_of_sale);
        $user->setSpecialization($specialization);

        $user->setUsername($username);
        $user->setPhone($phone);
        $user->setRegion($region);
        $user->setPointOfSale($point_of_sale);
        $user->setSpecialization($specialization);

        $user->save();
        return $user->getId();
    } else {
        return 'Пользоваьель не найден';
    }
});
/* ------------------------------------------------------------------ */

/* Поиск */

$klein->respond('GET', '/search/[:sender]', function ($request, $response) {

    return $result->toJSON();
});

/* ------------------------------------------------------------------- */
$klein->dispatch();
