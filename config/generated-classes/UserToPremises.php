<?php

use Base\UserToPremises as BaseUserToPremises;

/**
 * Skeleton subclass for representing a row from the 'user_to_premises' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class UserToPremises extends BaseUserToPremises
{

}
