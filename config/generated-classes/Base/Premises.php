<?php

namespace Base;

use \Premises as ChildPremises;
use \PremisesQuery as ChildPremisesQuery;
use \Routes as ChildRoutes;
use \RoutesQuery as ChildRoutesQuery;
use \Statuses as ChildStatuses;
use \StatusesQuery as ChildStatusesQuery;
use \TypesPremise as ChildTypesPremise;
use \TypesPremiseQuery as ChildTypesPremiseQuery;
use \UserToPremises as ChildUserToPremises;
use \UserToPremisesQuery as ChildUserToPremisesQuery;
use \Users as ChildUsers;
use \UsersQuery as ChildUsersQuery;
use \Exception;
use \PDO;
use Map\PremisesTableMap;
use Map\UserToPremisesTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveRecord\ActiveRecordInterface;
use Propel\Runtime\Collection\Collection;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\BadMethodCallException;
use Propel\Runtime\Exception\LogicException;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Parser\AbstractParser;

/**
 * Base class that represents a row from the 'premises' table.
 *
 *
 *
 * @package    propel.generator..Base
 */
abstract class Premises implements ActiveRecordInterface
{
    /**
     * TableMap class name
     */
    const TABLE_MAP = '\\Map\\PremisesTableMap';


    /**
     * attribute to determine if this object has previously been saved.
     * @var boolean
     */
    protected $new = true;

    /**
     * attribute to determine whether this object has been deleted.
     * @var boolean
     */
    protected $deleted = false;

    /**
     * The columns that have been modified in current object.
     * Tracking modified columns allows us to only update modified columns.
     * @var array
     */
    protected $modifiedColumns = array();

    /**
     * The (virtual) columns that are added at runtime
     * The formatters can add supplementary columns based on a resultset
     * @var array
     */
    protected $virtualColumns = array();

    /**
     * The value for the id field.
     *
     * @var        string
     */
    protected $id;

    /**
     * The value for the date_sender field.
     *
     * @var        string
     */
    protected $date_sender;

    /**
     * The value for the date_recipient field.
     *
     * @var        int
     */
    protected $date_recipient;

    /**
     * The value for the type_id field.
     *
     * @var        int
     */
    protected $type_id;

    /**
     * The value for the count field.
     *
     * @var        int
     */
    protected $count;

    /**
     * The value for the route_id field.
     *
     * @var        string
     */
    protected $route_id;

    /**
     * The value for the cost field.
     *
     * @var        int
     */
    protected $cost;

    /**
     * The value for the city_sender field.
     *
     * @var        string
     */
    protected $city_sender;

    /**
     * The value for the city_recipient field.
     *
     * @var        string
     */
    protected $city_recipient;

    /**
     * The value for the status_id field.
     *
     * @var        int
     */
    protected $status_id;

    /**
     * The value for the payer_id field.
     *
     * @var        string
     */
    protected $payer_id;

    /**
     * The value for the comment field.
     *
     * @var        string
     */
    protected $comment;

    /**
     * @var        ChildUsers
     */
    protected $aUsers;

    /**
     * @var        ChildRoutes
     */
    protected $aRoutes;

    /**
     * @var        ChildStatuses
     */
    protected $aStatuses;

    /**
     * @var        ChildTypesPremise
     */
    protected $aTypesPremise;

    /**
     * @var        ObjectCollection|ChildUserToPremises[] Collection to store aggregation of ChildUserToPremises objects.
     */
    protected $collUserToPremisess;
    protected $collUserToPremisessPartial;

    /**
     * Flag to prevent endless save loop, if this object is referenced
     * by another object which falls in this transaction.
     *
     * @var boolean
     */
    protected $alreadyInSave = false;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildUserToPremises[]
     */
    protected $userToPremisessScheduledForDeletion = null;

    /**
     * Initializes internal state of Base\Premises object.
     */
    public function __construct()
    {
    }

    /**
     * Returns whether the object has been modified.
     *
     * @return boolean True if the object has been modified.
     */
    public function isModified()
    {
        return !!$this->modifiedColumns;
    }

    /**
     * Has specified column been modified?
     *
     * @param  string  $col column fully qualified name (TableMap::TYPE_COLNAME), e.g. Book::AUTHOR_ID
     * @return boolean True if $col has been modified.
     */
    public function isColumnModified($col)
    {
        return $this->modifiedColumns && isset($this->modifiedColumns[$col]);
    }

    /**
     * Get the columns that have been modified in this object.
     * @return array A unique list of the modified column names for this object.
     */
    public function getModifiedColumns()
    {
        return $this->modifiedColumns ? array_keys($this->modifiedColumns) : [];
    }

    /**
     * Returns whether the object has ever been saved.  This will
     * be false, if the object was retrieved from storage or was created
     * and then saved.
     *
     * @return boolean true, if the object has never been persisted.
     */
    public function isNew()
    {
        return $this->new;
    }

    /**
     * Setter for the isNew attribute.  This method will be called
     * by Propel-generated children and objects.
     *
     * @param boolean $b the state of the object.
     */
    public function setNew($b)
    {
        $this->new = (boolean) $b;
    }

    /**
     * Whether this object has been deleted.
     * @return boolean The deleted state of this object.
     */
    public function isDeleted()
    {
        return $this->deleted;
    }

    /**
     * Specify whether this object has been deleted.
     * @param  boolean $b The deleted state of this object.
     * @return void
     */
    public function setDeleted($b)
    {
        $this->deleted = (boolean) $b;
    }

    /**
     * Sets the modified state for the object to be false.
     * @param  string $col If supplied, only the specified column is reset.
     * @return void
     */
    public function resetModified($col = null)
    {
        if (null !== $col) {
            if (isset($this->modifiedColumns[$col])) {
                unset($this->modifiedColumns[$col]);
            }
        } else {
            $this->modifiedColumns = array();
        }
    }

    /**
     * Compares this with another <code>Premises</code> instance.  If
     * <code>obj</code> is an instance of <code>Premises</code>, delegates to
     * <code>equals(Premises)</code>.  Otherwise, returns <code>false</code>.
     *
     * @param  mixed   $obj The object to compare to.
     * @return boolean Whether equal to the object specified.
     */
    public function equals($obj)
    {
        if (!$obj instanceof static) {
            return false;
        }

        if ($this === $obj) {
            return true;
        }

        if (null === $this->getPrimaryKey() || null === $obj->getPrimaryKey()) {
            return false;
        }

        return $this->getPrimaryKey() === $obj->getPrimaryKey();
    }

    /**
     * Get the associative array of the virtual columns in this object
     *
     * @return array
     */
    public function getVirtualColumns()
    {
        return $this->virtualColumns;
    }

    /**
     * Checks the existence of a virtual column in this object
     *
     * @param  string  $name The virtual column name
     * @return boolean
     */
    public function hasVirtualColumn($name)
    {
        return array_key_exists($name, $this->virtualColumns);
    }

    /**
     * Get the value of a virtual column in this object
     *
     * @param  string $name The virtual column name
     * @return mixed
     *
     * @throws PropelException
     */
    public function getVirtualColumn($name)
    {
        if (!$this->hasVirtualColumn($name)) {
            throw new PropelException(sprintf('Cannot get value of inexistent virtual column %s.', $name));
        }

        return $this->virtualColumns[$name];
    }

    /**
     * Set the value of a virtual column in this object
     *
     * @param string $name  The virtual column name
     * @param mixed  $value The value to give to the virtual column
     *
     * @return $this|Premises The current object, for fluid interface
     */
    public function setVirtualColumn($name, $value)
    {
        $this->virtualColumns[$name] = $value;

        return $this;
    }

    /**
     * Logs a message using Propel::log().
     *
     * @param  string  $msg
     * @param  int     $priority One of the Propel::LOG_* logging levels
     * @return boolean
     */
    protected function log($msg, $priority = Propel::LOG_INFO)
    {
        return Propel::log(get_class($this) . ': ' . $msg, $priority);
    }

    /**
     * Export the current object properties to a string, using a given parser format
     * <code>
     * $book = BookQuery::create()->findPk(9012);
     * echo $book->exportTo('JSON');
     *  => {"Id":9012,"Title":"Don Juan","ISBN":"0140422161","Price":12.99,"PublisherId":1234,"AuthorId":5678}');
     * </code>
     *
     * @param  mixed   $parser                 A AbstractParser instance, or a format name ('XML', 'YAML', 'JSON', 'CSV')
     * @param  boolean $includeLazyLoadColumns (optional) Whether to include lazy load(ed) columns. Defaults to TRUE.
     * @return string  The exported data
     */
    public function exportTo($parser, $includeLazyLoadColumns = true)
    {
        if (!$parser instanceof AbstractParser) {
            $parser = AbstractParser::getParser($parser);
        }

        return $parser->fromArray($this->toArray(TableMap::TYPE_PHPNAME, $includeLazyLoadColumns, array(), true));
    }

    /**
     * Clean up internal collections prior to serializing
     * Avoids recursive loops that turn into segmentation faults when serializing
     */
    public function __sleep()
    {
        $this->clearAllReferences();

        $cls = new \ReflectionClass($this);
        $propertyNames = [];
        $serializableProperties = array_diff($cls->getProperties(), $cls->getProperties(\ReflectionProperty::IS_STATIC));

        foreach($serializableProperties as $property) {
            $propertyNames[] = $property->getName();
        }

        return $propertyNames;
    }

    /**
     * Get the [id] column value.
     *
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get the [date_sender] column value.
     *
     * @return string
     */
    public function getDateSender()
    {
        return $this->date_sender;
    }

    /**
     * Get the [date_recipient] column value.
     *
     * @return int
     */
    public function getDateRecipient()
    {
        return $this->date_recipient;
    }

    /**
     * Get the [type_id] column value.
     *
     * @return int
     */
    public function getTypeId()
    {
        return $this->type_id;
    }

    /**
     * Get the [count] column value.
     *
     * @return int
     */
    public function getCount()
    {
        return $this->count;
    }

    /**
     * Get the [route_id] column value.
     *
     * @return string
     */
    public function getRouteId()
    {
        return $this->route_id;
    }

    /**
     * Get the [cost] column value.
     *
     * @return int
     */
    public function getCost()
    {
        return $this->cost;
    }

    /**
     * Get the [city_sender] column value.
     *
     * @return string
     */
    public function getCitySender()
    {
        return $this->city_sender;
    }

    /**
     * Get the [city_recipient] column value.
     *
     * @return string
     */
    public function getCityRecipient()
    {
        return $this->city_recipient;
    }

    /**
     * Get the [status_id] column value.
     *
     * @return int
     */
    public function getStatusId()
    {
        return $this->status_id;
    }

    /**
     * Get the [payer_id] column value.
     *
     * @return string
     */
    public function getPayerId()
    {
        return $this->payer_id;
    }

    /**
     * Get the [comment] column value.
     *
     * @return string
     */
    public function getComment()
    {
        return $this->comment;
    }

    /**
     * Set the value of [id] column.
     *
     * @param string $v new value
     * @return $this|\Premises The current object (for fluent API support)
     */
    public function setId($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->id !== $v) {
            $this->id = $v;
            $this->modifiedColumns[PremisesTableMap::COL_ID] = true;
        }

        return $this;
    } // setId()

    /**
     * Set the value of [date_sender] column.
     *
     * @param string $v new value
     * @return $this|\Premises The current object (for fluent API support)
     */
    public function setDateSender($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->date_sender !== $v) {
            $this->date_sender = $v;
            $this->modifiedColumns[PremisesTableMap::COL_DATE_SENDER] = true;
        }

        return $this;
    } // setDateSender()

    /**
     * Set the value of [date_recipient] column.
     *
     * @param int $v new value
     * @return $this|\Premises The current object (for fluent API support)
     */
    public function setDateRecipient($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->date_recipient !== $v) {
            $this->date_recipient = $v;
            $this->modifiedColumns[PremisesTableMap::COL_DATE_RECIPIENT] = true;
        }

        return $this;
    } // setDateRecipient()

    /**
     * Set the value of [type_id] column.
     *
     * @param int $v new value
     * @return $this|\Premises The current object (for fluent API support)
     */
    public function setTypeId($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->type_id !== $v) {
            $this->type_id = $v;
            $this->modifiedColumns[PremisesTableMap::COL_TYPE_ID] = true;
        }

        if ($this->aTypesPremise !== null && $this->aTypesPremise->getId() !== $v) {
            $this->aTypesPremise = null;
        }

        return $this;
    } // setTypeId()

    /**
     * Set the value of [count] column.
     *
     * @param int $v new value
     * @return $this|\Premises The current object (for fluent API support)
     */
    public function setCount($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->count !== $v) {
            $this->count = $v;
            $this->modifiedColumns[PremisesTableMap::COL_COUNT] = true;
        }

        return $this;
    } // setCount()

    /**
     * Set the value of [route_id] column.
     *
     * @param string $v new value
     * @return $this|\Premises The current object (for fluent API support)
     */
    public function setRouteId($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->route_id !== $v) {
            $this->route_id = $v;
            $this->modifiedColumns[PremisesTableMap::COL_ROUTE_ID] = true;
        }

        if ($this->aRoutes !== null && $this->aRoutes->getId() !== $v) {
            $this->aRoutes = null;
        }

        return $this;
    } // setRouteId()

    /**
     * Set the value of [cost] column.
     *
     * @param int $v new value
     * @return $this|\Premises The current object (for fluent API support)
     */
    public function setCost($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->cost !== $v) {
            $this->cost = $v;
            $this->modifiedColumns[PremisesTableMap::COL_COST] = true;
        }

        return $this;
    } // setCost()

    /**
     * Set the value of [city_sender] column.
     *
     * @param string $v new value
     * @return $this|\Premises The current object (for fluent API support)
     */
    public function setCitySender($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->city_sender !== $v) {
            $this->city_sender = $v;
            $this->modifiedColumns[PremisesTableMap::COL_CITY_SENDER] = true;
        }

        return $this;
    } // setCitySender()

    /**
     * Set the value of [city_recipient] column.
     *
     * @param string $v new value
     * @return $this|\Premises The current object (for fluent API support)
     */
    public function setCityRecipient($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->city_recipient !== $v) {
            $this->city_recipient = $v;
            $this->modifiedColumns[PremisesTableMap::COL_CITY_RECIPIENT] = true;
        }

        return $this;
    } // setCityRecipient()

    /**
     * Set the value of [status_id] column.
     *
     * @param int $v new value
     * @return $this|\Premises The current object (for fluent API support)
     */
    public function setStatusId($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->status_id !== $v) {
            $this->status_id = $v;
            $this->modifiedColumns[PremisesTableMap::COL_STATUS_ID] = true;
        }

        if ($this->aStatuses !== null && $this->aStatuses->getId() !== $v) {
            $this->aStatuses = null;
        }

        return $this;
    } // setStatusId()

    /**
     * Set the value of [payer_id] column.
     *
     * @param string $v new value
     * @return $this|\Premises The current object (for fluent API support)
     */
    public function setPayerId($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->payer_id !== $v) {
            $this->payer_id = $v;
            $this->modifiedColumns[PremisesTableMap::COL_PAYER_ID] = true;
        }

        if ($this->aUsers !== null && $this->aUsers->getId() !== $v) {
            $this->aUsers = null;
        }

        return $this;
    } // setPayerId()

    /**
     * Set the value of [comment] column.
     *
     * @param string $v new value
     * @return $this|\Premises The current object (for fluent API support)
     */
    public function setComment($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->comment !== $v) {
            $this->comment = $v;
            $this->modifiedColumns[PremisesTableMap::COL_COMMENT] = true;
        }

        return $this;
    } // setComment()

    /**
     * Indicates whether the columns in this object are only set to default values.
     *
     * This method can be used in conjunction with isModified() to indicate whether an object is both
     * modified _and_ has some values set which are non-default.
     *
     * @return boolean Whether the columns in this object are only been set with default values.
     */
    public function hasOnlyDefaultValues()
    {
        // otherwise, everything was equal, so return TRUE
        return true;
    } // hasOnlyDefaultValues()

    /**
     * Hydrates (populates) the object variables with values from the database resultset.
     *
     * An offset (0-based "start column") is specified so that objects can be hydrated
     * with a subset of the columns in the resultset rows.  This is needed, for example,
     * for results of JOIN queries where the resultset row includes columns from two or
     * more tables.
     *
     * @param array   $row       The row returned by DataFetcher->fetch().
     * @param int     $startcol  0-based offset column which indicates which restultset column to start with.
     * @param boolean $rehydrate Whether this object is being re-hydrated from the database.
     * @param string  $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                  One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                            TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @return int             next starting column
     * @throws PropelException - Any caught Exception will be rewrapped as a PropelException.
     */
    public function hydrate($row, $startcol = 0, $rehydrate = false, $indexType = TableMap::TYPE_NUM)
    {
        try {

            $col = $row[TableMap::TYPE_NUM == $indexType ? 0 + $startcol : PremisesTableMap::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)];
            $this->id = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 1 + $startcol : PremisesTableMap::translateFieldName('DateSender', TableMap::TYPE_PHPNAME, $indexType)];
            $this->date_sender = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 2 + $startcol : PremisesTableMap::translateFieldName('DateRecipient', TableMap::TYPE_PHPNAME, $indexType)];
            $this->date_recipient = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 3 + $startcol : PremisesTableMap::translateFieldName('TypeId', TableMap::TYPE_PHPNAME, $indexType)];
            $this->type_id = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 4 + $startcol : PremisesTableMap::translateFieldName('Count', TableMap::TYPE_PHPNAME, $indexType)];
            $this->count = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 5 + $startcol : PremisesTableMap::translateFieldName('RouteId', TableMap::TYPE_PHPNAME, $indexType)];
            $this->route_id = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 6 + $startcol : PremisesTableMap::translateFieldName('Cost', TableMap::TYPE_PHPNAME, $indexType)];
            $this->cost = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 7 + $startcol : PremisesTableMap::translateFieldName('CitySender', TableMap::TYPE_PHPNAME, $indexType)];
            $this->city_sender = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 8 + $startcol : PremisesTableMap::translateFieldName('CityRecipient', TableMap::TYPE_PHPNAME, $indexType)];
            $this->city_recipient = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 9 + $startcol : PremisesTableMap::translateFieldName('StatusId', TableMap::TYPE_PHPNAME, $indexType)];
            $this->status_id = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 10 + $startcol : PremisesTableMap::translateFieldName('PayerId', TableMap::TYPE_PHPNAME, $indexType)];
            $this->payer_id = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 11 + $startcol : PremisesTableMap::translateFieldName('Comment', TableMap::TYPE_PHPNAME, $indexType)];
            $this->comment = (null !== $col) ? (string) $col : null;
            $this->resetModified();

            $this->setNew(false);

            if ($rehydrate) {
                $this->ensureConsistency();
            }

            return $startcol + 12; // 12 = PremisesTableMap::NUM_HYDRATE_COLUMNS.

        } catch (Exception $e) {
            throw new PropelException(sprintf('Error populating %s object', '\\Premises'), 0, $e);
        }
    }

    /**
     * Checks and repairs the internal consistency of the object.
     *
     * This method is executed after an already-instantiated object is re-hydrated
     * from the database.  It exists to check any foreign keys to make sure that
     * the objects related to the current object are correct based on foreign key.
     *
     * You can override this method in the stub class, but you should always invoke
     * the base method from the overridden method (i.e. parent::ensureConsistency()),
     * in case your model changes.
     *
     * @throws PropelException
     */
    public function ensureConsistency()
    {
        if ($this->aTypesPremise !== null && $this->type_id !== $this->aTypesPremise->getId()) {
            $this->aTypesPremise = null;
        }
        if ($this->aRoutes !== null && $this->route_id !== $this->aRoutes->getId()) {
            $this->aRoutes = null;
        }
        if ($this->aStatuses !== null && $this->status_id !== $this->aStatuses->getId()) {
            $this->aStatuses = null;
        }
        if ($this->aUsers !== null && $this->payer_id !== $this->aUsers->getId()) {
            $this->aUsers = null;
        }
    } // ensureConsistency

    /**
     * Reloads this object from datastore based on primary key and (optionally) resets all associated objects.
     *
     * This will only work if the object has been saved and has a valid primary key set.
     *
     * @param      boolean $deep (optional) Whether to also de-associated any related objects.
     * @param      ConnectionInterface $con (optional) The ConnectionInterface connection to use.
     * @return void
     * @throws PropelException - if this object is deleted, unsaved or doesn't have pk match in db
     */
    public function reload($deep = false, ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("Cannot reload a deleted object.");
        }

        if ($this->isNew()) {
            throw new PropelException("Cannot reload an unsaved object.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(PremisesTableMap::DATABASE_NAME);
        }

        // We don't need to alter the object instance pool; we're just modifying this instance
        // already in the pool.

        $dataFetcher = ChildPremisesQuery::create(null, $this->buildPkeyCriteria())->setFormatter(ModelCriteria::FORMAT_STATEMENT)->find($con);
        $row = $dataFetcher->fetch();
        $dataFetcher->close();
        if (!$row) {
            throw new PropelException('Cannot find matching row in the database to reload object values.');
        }
        $this->hydrate($row, 0, true, $dataFetcher->getIndexType()); // rehydrate

        if ($deep) {  // also de-associate any related objects?

            $this->aUsers = null;
            $this->aRoutes = null;
            $this->aStatuses = null;
            $this->aTypesPremise = null;
            $this->collUserToPremisess = null;

        } // if (deep)
    }

    /**
     * Removes this object from datastore and sets delete attribute.
     *
     * @param      ConnectionInterface $con
     * @return void
     * @throws PropelException
     * @see Premises::setDeleted()
     * @see Premises::isDeleted()
     */
    public function delete(ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("This object has already been deleted.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getWriteConnection(PremisesTableMap::DATABASE_NAME);
        }

        $con->transaction(function () use ($con) {
            $deleteQuery = ChildPremisesQuery::create()
                ->filterByPrimaryKey($this->getPrimaryKey());
            $ret = $this->preDelete($con);
            if ($ret) {
                $deleteQuery->delete($con);
                $this->postDelete($con);
                $this->setDeleted(true);
            }
        });
    }

    /**
     * Persists this object to the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All modified related objects will also be persisted in the doSave()
     * method.  This method wraps all precipitate database operations in a
     * single transaction.
     *
     * @param      ConnectionInterface $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see doSave()
     */
    public function save(ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("You cannot save an object that has been deleted.");
        }

        if ($this->alreadyInSave) {
            return 0;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getWriteConnection(PremisesTableMap::DATABASE_NAME);
        }

        return $con->transaction(function () use ($con) {
            $ret = $this->preSave($con);
            $isInsert = $this->isNew();
            if ($isInsert) {
                $ret = $ret && $this->preInsert($con);
            } else {
                $ret = $ret && $this->preUpdate($con);
            }
            if ($ret) {
                $affectedRows = $this->doSave($con);
                if ($isInsert) {
                    $this->postInsert($con);
                } else {
                    $this->postUpdate($con);
                }
                $this->postSave($con);
                PremisesTableMap::addInstanceToPool($this);
            } else {
                $affectedRows = 0;
            }

            return $affectedRows;
        });
    }

    /**
     * Performs the work of inserting or updating the row in the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All related objects are also updated in this method.
     *
     * @param      ConnectionInterface $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see save()
     */
    protected function doSave(ConnectionInterface $con)
    {
        $affectedRows = 0; // initialize var to track total num of affected rows
        if (!$this->alreadyInSave) {
            $this->alreadyInSave = true;

            // We call the save method on the following object(s) if they
            // were passed to this object by their corresponding set
            // method.  This object relates to these object(s) by a
            // foreign key reference.

            if ($this->aUsers !== null) {
                if ($this->aUsers->isModified() || $this->aUsers->isNew()) {
                    $affectedRows += $this->aUsers->save($con);
                }
                $this->setUsers($this->aUsers);
            }

            if ($this->aRoutes !== null) {
                if ($this->aRoutes->isModified() || $this->aRoutes->isNew()) {
                    $affectedRows += $this->aRoutes->save($con);
                }
                $this->setRoutes($this->aRoutes);
            }

            if ($this->aStatuses !== null) {
                if ($this->aStatuses->isModified() || $this->aStatuses->isNew()) {
                    $affectedRows += $this->aStatuses->save($con);
                }
                $this->setStatuses($this->aStatuses);
            }

            if ($this->aTypesPremise !== null) {
                if ($this->aTypesPremise->isModified() || $this->aTypesPremise->isNew()) {
                    $affectedRows += $this->aTypesPremise->save($con);
                }
                $this->setTypesPremise($this->aTypesPremise);
            }

            if ($this->isNew() || $this->isModified()) {
                // persist changes
                if ($this->isNew()) {
                    $this->doInsert($con);
                    $affectedRows += 1;
                } else {
                    $affectedRows += $this->doUpdate($con);
                }
                $this->resetModified();
            }

            if ($this->userToPremisessScheduledForDeletion !== null) {
                if (!$this->userToPremisessScheduledForDeletion->isEmpty()) {
                    \UserToPremisesQuery::create()
                        ->filterByPrimaryKeys($this->userToPremisessScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->userToPremisessScheduledForDeletion = null;
                }
            }

            if ($this->collUserToPremisess !== null) {
                foreach ($this->collUserToPremisess as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            $this->alreadyInSave = false;

        }

        return $affectedRows;
    } // doSave()

    /**
     * Insert the row in the database.
     *
     * @param      ConnectionInterface $con
     *
     * @throws PropelException
     * @see doSave()
     */
    protected function doInsert(ConnectionInterface $con)
    {
        $modifiedColumns = array();
        $index = 0;

        $this->modifiedColumns[PremisesTableMap::COL_ID] = true;
        if (null !== $this->id) {
            throw new PropelException('Cannot insert a value for auto-increment primary key (' . PremisesTableMap::COL_ID . ')');
        }

         // check the columns in natural order for more readable SQL queries
        if ($this->isColumnModified(PremisesTableMap::COL_ID)) {
            $modifiedColumns[':p' . $index++]  = 'id';
        }
        if ($this->isColumnModified(PremisesTableMap::COL_DATE_SENDER)) {
            $modifiedColumns[':p' . $index++]  = 'date_sender';
        }
        if ($this->isColumnModified(PremisesTableMap::COL_DATE_RECIPIENT)) {
            $modifiedColumns[':p' . $index++]  = 'date_recipient';
        }
        if ($this->isColumnModified(PremisesTableMap::COL_TYPE_ID)) {
            $modifiedColumns[':p' . $index++]  = 'type_id';
        }
        if ($this->isColumnModified(PremisesTableMap::COL_COUNT)) {
            $modifiedColumns[':p' . $index++]  = 'count';
        }
        if ($this->isColumnModified(PremisesTableMap::COL_ROUTE_ID)) {
            $modifiedColumns[':p' . $index++]  = 'route_id';
        }
        if ($this->isColumnModified(PremisesTableMap::COL_COST)) {
            $modifiedColumns[':p' . $index++]  = 'cost';
        }
        if ($this->isColumnModified(PremisesTableMap::COL_CITY_SENDER)) {
            $modifiedColumns[':p' . $index++]  = 'city_sender';
        }
        if ($this->isColumnModified(PremisesTableMap::COL_CITY_RECIPIENT)) {
            $modifiedColumns[':p' . $index++]  = 'city_recipient';
        }
        if ($this->isColumnModified(PremisesTableMap::COL_STATUS_ID)) {
            $modifiedColumns[':p' . $index++]  = 'status_id';
        }
        if ($this->isColumnModified(PremisesTableMap::COL_PAYER_ID)) {
            $modifiedColumns[':p' . $index++]  = 'payer_id';
        }
        if ($this->isColumnModified(PremisesTableMap::COL_COMMENT)) {
            $modifiedColumns[':p' . $index++]  = 'comment';
        }

        $sql = sprintf(
            'INSERT INTO premises (%s) VALUES (%s)',
            implode(', ', $modifiedColumns),
            implode(', ', array_keys($modifiedColumns))
        );

        try {
            $stmt = $con->prepare($sql);
            foreach ($modifiedColumns as $identifier => $columnName) {
                switch ($columnName) {
                    case 'id':
                        $stmt->bindValue($identifier, $this->id, PDO::PARAM_INT);
                        break;
                    case 'date_sender':
                        $stmt->bindValue($identifier, $this->date_sender, PDO::PARAM_INT);
                        break;
                    case 'date_recipient':
                        $stmt->bindValue($identifier, $this->date_recipient, PDO::PARAM_INT);
                        break;
                    case 'type_id':
                        $stmt->bindValue($identifier, $this->type_id, PDO::PARAM_INT);
                        break;
                    case 'count':
                        $stmt->bindValue($identifier, $this->count, PDO::PARAM_INT);
                        break;
                    case 'route_id':
                        $stmt->bindValue($identifier, $this->route_id, PDO::PARAM_INT);
                        break;
                    case 'cost':
                        $stmt->bindValue($identifier, $this->cost, PDO::PARAM_INT);
                        break;
                    case 'city_sender':
                        $stmt->bindValue($identifier, $this->city_sender, PDO::PARAM_STR);
                        break;
                    case 'city_recipient':
                        $stmt->bindValue($identifier, $this->city_recipient, PDO::PARAM_STR);
                        break;
                    case 'status_id':
                        $stmt->bindValue($identifier, $this->status_id, PDO::PARAM_INT);
                        break;
                    case 'payer_id':
                        $stmt->bindValue($identifier, $this->payer_id, PDO::PARAM_INT);
                        break;
                    case 'comment':
                        $stmt->bindValue($identifier, $this->comment, PDO::PARAM_STR);
                        break;
                }
            }
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute INSERT statement [%s]', $sql), 0, $e);
        }

        try {
            $pk = $con->lastInsertId();
        } catch (Exception $e) {
            throw new PropelException('Unable to get autoincrement id.', 0, $e);
        }
        $this->setId($pk);

        $this->setNew(false);
    }

    /**
     * Update the row in the database.
     *
     * @param      ConnectionInterface $con
     *
     * @return Integer Number of updated rows
     * @see doSave()
     */
    protected function doUpdate(ConnectionInterface $con)
    {
        $selectCriteria = $this->buildPkeyCriteria();
        $valuesCriteria = $this->buildCriteria();

        return $selectCriteria->doUpdate($valuesCriteria, $con);
    }

    /**
     * Retrieves a field from the object by name passed in as a string.
     *
     * @param      string $name name
     * @param      string $type The type of fieldname the $name is of:
     *                     one of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                     TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                     Defaults to TableMap::TYPE_PHPNAME.
     * @return mixed Value of field.
     */
    public function getByName($name, $type = TableMap::TYPE_PHPNAME)
    {
        $pos = PremisesTableMap::translateFieldName($name, $type, TableMap::TYPE_NUM);
        $field = $this->getByPosition($pos);

        return $field;
    }

    /**
     * Retrieves a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param      int $pos position in xml schema
     * @return mixed Value of field at $pos
     */
    public function getByPosition($pos)
    {
        switch ($pos) {
            case 0:
                return $this->getId();
                break;
            case 1:
                return $this->getDateSender();
                break;
            case 2:
                return $this->getDateRecipient();
                break;
            case 3:
                return $this->getTypeId();
                break;
            case 4:
                return $this->getCount();
                break;
            case 5:
                return $this->getRouteId();
                break;
            case 6:
                return $this->getCost();
                break;
            case 7:
                return $this->getCitySender();
                break;
            case 8:
                return $this->getCityRecipient();
                break;
            case 9:
                return $this->getStatusId();
                break;
            case 10:
                return $this->getPayerId();
                break;
            case 11:
                return $this->getComment();
                break;
            default:
                return null;
                break;
        } // switch()
    }

    /**
     * Exports the object as an array.
     *
     * You can specify the key type of the array by passing one of the class
     * type constants.
     *
     * @param     string  $keyType (optional) One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     *                    TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                    Defaults to TableMap::TYPE_PHPNAME.
     * @param     boolean $includeLazyLoadColumns (optional) Whether to include lazy loaded columns. Defaults to TRUE.
     * @param     array $alreadyDumpedObjects List of objects to skip to avoid recursion
     * @param     boolean $includeForeignObjects (optional) Whether to include hydrated related objects. Default to FALSE.
     *
     * @return array an associative array containing the field names (as keys) and field values
     */
    public function toArray($keyType = TableMap::TYPE_PHPNAME, $includeLazyLoadColumns = true, $alreadyDumpedObjects = array(), $includeForeignObjects = false)
    {

        if (isset($alreadyDumpedObjects['Premises'][$this->hashCode()])) {
            return '*RECURSION*';
        }
        $alreadyDumpedObjects['Premises'][$this->hashCode()] = true;
        $keys = PremisesTableMap::getFieldNames($keyType);
        $result = array(
            $keys[0] => $this->getId(),
            $keys[1] => $this->getDateSender(),
            $keys[2] => $this->getDateRecipient(),
            $keys[3] => $this->getTypeId(),
            $keys[4] => $this->getCount(),
            $keys[5] => $this->getRouteId(),
            $keys[6] => $this->getCost(),
            $keys[7] => $this->getCitySender(),
            $keys[8] => $this->getCityRecipient(),
            $keys[9] => $this->getStatusId(),
            $keys[10] => $this->getPayerId(),
            $keys[11] => $this->getComment(),
        );
        $virtualColumns = $this->virtualColumns;
        foreach ($virtualColumns as $key => $virtualColumn) {
            $result[$key] = $virtualColumn;
        }

        if ($includeForeignObjects) {
            if (null !== $this->aUsers) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'users';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'users';
                        break;
                    default:
                        $key = 'Users';
                }

                $result[$key] = $this->aUsers->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aRoutes) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'routes';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'routes';
                        break;
                    default:
                        $key = 'Routes';
                }

                $result[$key] = $this->aRoutes->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aStatuses) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'statuses';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'statuses';
                        break;
                    default:
                        $key = 'Statuses';
                }

                $result[$key] = $this->aStatuses->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aTypesPremise) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'typesPremise';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'types_premise';
                        break;
                    default:
                        $key = 'TypesPremise';
                }

                $result[$key] = $this->aTypesPremise->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->collUserToPremisess) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'userToPremisess';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'user_to_premisess';
                        break;
                    default:
                        $key = 'UserToPremisess';
                }

                $result[$key] = $this->collUserToPremisess->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
        }

        return $result;
    }

    /**
     * Sets a field from the object by name passed in as a string.
     *
     * @param  string $name
     * @param  mixed  $value field value
     * @param  string $type The type of fieldname the $name is of:
     *                one of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                Defaults to TableMap::TYPE_PHPNAME.
     * @return $this|\Premises
     */
    public function setByName($name, $value, $type = TableMap::TYPE_PHPNAME)
    {
        $pos = PremisesTableMap::translateFieldName($name, $type, TableMap::TYPE_NUM);

        return $this->setByPosition($pos, $value);
    }

    /**
     * Sets a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param  int $pos position in xml schema
     * @param  mixed $value field value
     * @return $this|\Premises
     */
    public function setByPosition($pos, $value)
    {
        switch ($pos) {
            case 0:
                $this->setId($value);
                break;
            case 1:
                $this->setDateSender($value);
                break;
            case 2:
                $this->setDateRecipient($value);
                break;
            case 3:
                $this->setTypeId($value);
                break;
            case 4:
                $this->setCount($value);
                break;
            case 5:
                $this->setRouteId($value);
                break;
            case 6:
                $this->setCost($value);
                break;
            case 7:
                $this->setCitySender($value);
                break;
            case 8:
                $this->setCityRecipient($value);
                break;
            case 9:
                $this->setStatusId($value);
                break;
            case 10:
                $this->setPayerId($value);
                break;
            case 11:
                $this->setComment($value);
                break;
        } // switch()

        return $this;
    }

    /**
     * Populates the object using an array.
     *
     * This is particularly useful when populating an object from one of the
     * request arrays (e.g. $_POST).  This method goes through the column
     * names, checking to see whether a matching key exists in populated
     * array. If so the setByName() method is called for that column.
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     * TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     * The default key type is the column's TableMap::TYPE_PHPNAME.
     *
     * @param      array  $arr     An array to populate the object from.
     * @param      string $keyType The type of keys the array uses.
     * @return void
     */
    public function fromArray($arr, $keyType = TableMap::TYPE_PHPNAME)
    {
        $keys = PremisesTableMap::getFieldNames($keyType);

        if (array_key_exists($keys[0], $arr)) {
            $this->setId($arr[$keys[0]]);
        }
        if (array_key_exists($keys[1], $arr)) {
            $this->setDateSender($arr[$keys[1]]);
        }
        if (array_key_exists($keys[2], $arr)) {
            $this->setDateRecipient($arr[$keys[2]]);
        }
        if (array_key_exists($keys[3], $arr)) {
            $this->setTypeId($arr[$keys[3]]);
        }
        if (array_key_exists($keys[4], $arr)) {
            $this->setCount($arr[$keys[4]]);
        }
        if (array_key_exists($keys[5], $arr)) {
            $this->setRouteId($arr[$keys[5]]);
        }
        if (array_key_exists($keys[6], $arr)) {
            $this->setCost($arr[$keys[6]]);
        }
        if (array_key_exists($keys[7], $arr)) {
            $this->setCitySender($arr[$keys[7]]);
        }
        if (array_key_exists($keys[8], $arr)) {
            $this->setCityRecipient($arr[$keys[8]]);
        }
        if (array_key_exists($keys[9], $arr)) {
            $this->setStatusId($arr[$keys[9]]);
        }
        if (array_key_exists($keys[10], $arr)) {
            $this->setPayerId($arr[$keys[10]]);
        }
        if (array_key_exists($keys[11], $arr)) {
            $this->setComment($arr[$keys[11]]);
        }
    }

     /**
     * Populate the current object from a string, using a given parser format
     * <code>
     * $book = new Book();
     * $book->importFrom('JSON', '{"Id":9012,"Title":"Don Juan","ISBN":"0140422161","Price":12.99,"PublisherId":1234,"AuthorId":5678}');
     * </code>
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     * TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     * The default key type is the column's TableMap::TYPE_PHPNAME.
     *
     * @param mixed $parser A AbstractParser instance,
     *                       or a format name ('XML', 'YAML', 'JSON', 'CSV')
     * @param string $data The source data to import from
     * @param string $keyType The type of keys the array uses.
     *
     * @return $this|\Premises The current object, for fluid interface
     */
    public function importFrom($parser, $data, $keyType = TableMap::TYPE_PHPNAME)
    {
        if (!$parser instanceof AbstractParser) {
            $parser = AbstractParser::getParser($parser);
        }

        $this->fromArray($parser->toArray($data), $keyType);

        return $this;
    }

    /**
     * Build a Criteria object containing the values of all modified columns in this object.
     *
     * @return Criteria The Criteria object containing all modified values.
     */
    public function buildCriteria()
    {
        $criteria = new Criteria(PremisesTableMap::DATABASE_NAME);

        if ($this->isColumnModified(PremisesTableMap::COL_ID)) {
            $criteria->add(PremisesTableMap::COL_ID, $this->id);
        }
        if ($this->isColumnModified(PremisesTableMap::COL_DATE_SENDER)) {
            $criteria->add(PremisesTableMap::COL_DATE_SENDER, $this->date_sender);
        }
        if ($this->isColumnModified(PremisesTableMap::COL_DATE_RECIPIENT)) {
            $criteria->add(PremisesTableMap::COL_DATE_RECIPIENT, $this->date_recipient);
        }
        if ($this->isColumnModified(PremisesTableMap::COL_TYPE_ID)) {
            $criteria->add(PremisesTableMap::COL_TYPE_ID, $this->type_id);
        }
        if ($this->isColumnModified(PremisesTableMap::COL_COUNT)) {
            $criteria->add(PremisesTableMap::COL_COUNT, $this->count);
        }
        if ($this->isColumnModified(PremisesTableMap::COL_ROUTE_ID)) {
            $criteria->add(PremisesTableMap::COL_ROUTE_ID, $this->route_id);
        }
        if ($this->isColumnModified(PremisesTableMap::COL_COST)) {
            $criteria->add(PremisesTableMap::COL_COST, $this->cost);
        }
        if ($this->isColumnModified(PremisesTableMap::COL_CITY_SENDER)) {
            $criteria->add(PremisesTableMap::COL_CITY_SENDER, $this->city_sender);
        }
        if ($this->isColumnModified(PremisesTableMap::COL_CITY_RECIPIENT)) {
            $criteria->add(PremisesTableMap::COL_CITY_RECIPIENT, $this->city_recipient);
        }
        if ($this->isColumnModified(PremisesTableMap::COL_STATUS_ID)) {
            $criteria->add(PremisesTableMap::COL_STATUS_ID, $this->status_id);
        }
        if ($this->isColumnModified(PremisesTableMap::COL_PAYER_ID)) {
            $criteria->add(PremisesTableMap::COL_PAYER_ID, $this->payer_id);
        }
        if ($this->isColumnModified(PremisesTableMap::COL_COMMENT)) {
            $criteria->add(PremisesTableMap::COL_COMMENT, $this->comment);
        }

        return $criteria;
    }

    /**
     * Builds a Criteria object containing the primary key for this object.
     *
     * Unlike buildCriteria() this method includes the primary key values regardless
     * of whether or not they have been modified.
     *
     * @throws LogicException if no primary key is defined
     *
     * @return Criteria The Criteria object containing value(s) for primary key(s).
     */
    public function buildPkeyCriteria()
    {
        $criteria = ChildPremisesQuery::create();
        $criteria->add(PremisesTableMap::COL_ID, $this->id);

        return $criteria;
    }

    /**
     * If the primary key is not null, return the hashcode of the
     * primary key. Otherwise, return the hash code of the object.
     *
     * @return int Hashcode
     */
    public function hashCode()
    {
        $validPk = null !== $this->getId();

        $validPrimaryKeyFKs = 0;
        $primaryKeyFKs = [];

        if ($validPk) {
            return crc32(json_encode($this->getPrimaryKey(), JSON_UNESCAPED_UNICODE));
        } elseif ($validPrimaryKeyFKs) {
            return crc32(json_encode($primaryKeyFKs, JSON_UNESCAPED_UNICODE));
        }

        return spl_object_hash($this);
    }

    /**
     * Returns the primary key for this object (row).
     * @return string
     */
    public function getPrimaryKey()
    {
        return $this->getId();
    }

    /**
     * Generic method to set the primary key (id column).
     *
     * @param       string $key Primary key.
     * @return void
     */
    public function setPrimaryKey($key)
    {
        $this->setId($key);
    }

    /**
     * Returns true if the primary key for this object is null.
     * @return boolean
     */
    public function isPrimaryKeyNull()
    {
        return null === $this->getId();
    }

    /**
     * Sets contents of passed object to values from current object.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param      object $copyObj An object of \Premises (or compatible) type.
     * @param      boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @param      boolean $makeNew Whether to reset autoincrement PKs and make the object new.
     * @throws PropelException
     */
    public function copyInto($copyObj, $deepCopy = false, $makeNew = true)
    {
        $copyObj->setDateSender($this->getDateSender());
        $copyObj->setDateRecipient($this->getDateRecipient());
        $copyObj->setTypeId($this->getTypeId());
        $copyObj->setCount($this->getCount());
        $copyObj->setRouteId($this->getRouteId());
        $copyObj->setCost($this->getCost());
        $copyObj->setCitySender($this->getCitySender());
        $copyObj->setCityRecipient($this->getCityRecipient());
        $copyObj->setStatusId($this->getStatusId());
        $copyObj->setPayerId($this->getPayerId());
        $copyObj->setComment($this->getComment());

        if ($deepCopy) {
            // important: temporarily setNew(false) because this affects the behavior of
            // the getter/setter methods for fkey referrer objects.
            $copyObj->setNew(false);

            foreach ($this->getUserToPremisess() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addUserToPremises($relObj->copy($deepCopy));
                }
            }

        } // if ($deepCopy)

        if ($makeNew) {
            $copyObj->setNew(true);
            $copyObj->setId(NULL); // this is a auto-increment column, so set to default value
        }
    }

    /**
     * Makes a copy of this object that will be inserted as a new row in table when saved.
     * It creates a new object filling in the simple attributes, but skipping any primary
     * keys that are defined for the table.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param  boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @return \Premises Clone of current object.
     * @throws PropelException
     */
    public function copy($deepCopy = false)
    {
        // we use get_class(), because this might be a subclass
        $clazz = get_class($this);
        $copyObj = new $clazz();
        $this->copyInto($copyObj, $deepCopy);

        return $copyObj;
    }

    /**
     * Declares an association between this object and a ChildUsers object.
     *
     * @param  ChildUsers $v
     * @return $this|\Premises The current object (for fluent API support)
     * @throws PropelException
     */
    public function setUsers(ChildUsers $v = null)
    {
        if ($v === null) {
            $this->setPayerId(NULL);
        } else {
            $this->setPayerId($v->getId());
        }

        $this->aUsers = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the ChildUsers object, it will not be re-added.
        if ($v !== null) {
            $v->addPremises($this);
        }


        return $this;
    }


    /**
     * Get the associated ChildUsers object
     *
     * @param  ConnectionInterface $con Optional Connection object.
     * @return ChildUsers The associated ChildUsers object.
     * @throws PropelException
     */
    public function getUsers(ConnectionInterface $con = null)
    {
        if ($this->aUsers === null && (($this->payer_id !== "" && $this->payer_id !== null))) {
            $this->aUsers = ChildUsersQuery::create()->findPk($this->payer_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aUsers->addPremisess($this);
             */
        }

        return $this->aUsers;
    }

    /**
     * Declares an association between this object and a ChildRoutes object.
     *
     * @param  ChildRoutes $v
     * @return $this|\Premises The current object (for fluent API support)
     * @throws PropelException
     */
    public function setRoutes(ChildRoutes $v = null)
    {
        if ($v === null) {
            $this->setRouteId(NULL);
        } else {
            $this->setRouteId($v->getId());
        }

        $this->aRoutes = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the ChildRoutes object, it will not be re-added.
        if ($v !== null) {
            $v->addPremises($this);
        }


        return $this;
    }


    /**
     * Get the associated ChildRoutes object
     *
     * @param  ConnectionInterface $con Optional Connection object.
     * @return ChildRoutes The associated ChildRoutes object.
     * @throws PropelException
     */
    public function getRoutes(ConnectionInterface $con = null)
    {
        if ($this->aRoutes === null && (($this->route_id !== "" && $this->route_id !== null))) {
            $this->aRoutes = ChildRoutesQuery::create()->findPk($this->route_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aRoutes->addPremisess($this);
             */
        }

        return $this->aRoutes;
    }

    /**
     * Declares an association between this object and a ChildStatuses object.
     *
     * @param  ChildStatuses $v
     * @return $this|\Premises The current object (for fluent API support)
     * @throws PropelException
     */
    public function setStatuses(ChildStatuses $v = null)
    {
        if ($v === null) {
            $this->setStatusId(NULL);
        } else {
            $this->setStatusId($v->getId());
        }

        $this->aStatuses = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the ChildStatuses object, it will not be re-added.
        if ($v !== null) {
            $v->addPremises($this);
        }


        return $this;
    }


    /**
     * Get the associated ChildStatuses object
     *
     * @param  ConnectionInterface $con Optional Connection object.
     * @return ChildStatuses The associated ChildStatuses object.
     * @throws PropelException
     */
    public function getStatuses(ConnectionInterface $con = null)
    {
        if ($this->aStatuses === null && ($this->status_id != 0)) {
            $this->aStatuses = ChildStatusesQuery::create()->findPk($this->status_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aStatuses->addPremisess($this);
             */
        }

        return $this->aStatuses;
    }

    /**
     * Declares an association between this object and a ChildTypesPremise object.
     *
     * @param  ChildTypesPremise $v
     * @return $this|\Premises The current object (for fluent API support)
     * @throws PropelException
     */
    public function setTypesPremise(ChildTypesPremise $v = null)
    {
        if ($v === null) {
            $this->setTypeId(NULL);
        } else {
            $this->setTypeId($v->getId());
        }

        $this->aTypesPremise = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the ChildTypesPremise object, it will not be re-added.
        if ($v !== null) {
            $v->addPremises($this);
        }


        return $this;
    }


    /**
     * Get the associated ChildTypesPremise object
     *
     * @param  ConnectionInterface $con Optional Connection object.
     * @return ChildTypesPremise The associated ChildTypesPremise object.
     * @throws PropelException
     */
    public function getTypesPremise(ConnectionInterface $con = null)
    {
        if ($this->aTypesPremise === null && ($this->type_id != 0)) {
            $this->aTypesPremise = ChildTypesPremiseQuery::create()->findPk($this->type_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aTypesPremise->addPremisess($this);
             */
        }

        return $this->aTypesPremise;
    }


    /**
     * Initializes a collection based on the name of a relation.
     * Avoids crafting an 'init[$relationName]s' method name
     * that wouldn't work when StandardEnglishPluralizer is used.
     *
     * @param      string $relationName The name of the relation to initialize
     * @return void
     */
    public function initRelation($relationName)
    {
        if ('UserToPremises' == $relationName) {
            $this->initUserToPremisess();
            return;
        }
    }

    /**
     * Clears out the collUserToPremisess collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addUserToPremisess()
     */
    public function clearUserToPremisess()
    {
        $this->collUserToPremisess = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collUserToPremisess collection loaded partially.
     */
    public function resetPartialUserToPremisess($v = true)
    {
        $this->collUserToPremisessPartial = $v;
    }

    /**
     * Initializes the collUserToPremisess collection.
     *
     * By default this just sets the collUserToPremisess collection to an empty array (like clearcollUserToPremisess());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initUserToPremisess($overrideExisting = true)
    {
        if (null !== $this->collUserToPremisess && !$overrideExisting) {
            return;
        }

        $collectionClassName = UserToPremisesTableMap::getTableMap()->getCollectionClassName();

        $this->collUserToPremisess = new $collectionClassName;
        $this->collUserToPremisess->setModel('\UserToPremises');
    }

    /**
     * Gets an array of ChildUserToPremises objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildPremises is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildUserToPremises[] List of ChildUserToPremises objects
     * @throws PropelException
     */
    public function getUserToPremisess(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collUserToPremisessPartial && !$this->isNew();
        if (null === $this->collUserToPremisess || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collUserToPremisess) {
                // return empty collection
                $this->initUserToPremisess();
            } else {
                $collUserToPremisess = ChildUserToPremisesQuery::create(null, $criteria)
                    ->filterByPremises($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collUserToPremisessPartial && count($collUserToPremisess)) {
                        $this->initUserToPremisess(false);

                        foreach ($collUserToPremisess as $obj) {
                            if (false == $this->collUserToPremisess->contains($obj)) {
                                $this->collUserToPremisess->append($obj);
                            }
                        }

                        $this->collUserToPremisessPartial = true;
                    }

                    return $collUserToPremisess;
                }

                if ($partial && $this->collUserToPremisess) {
                    foreach ($this->collUserToPremisess as $obj) {
                        if ($obj->isNew()) {
                            $collUserToPremisess[] = $obj;
                        }
                    }
                }

                $this->collUserToPremisess = $collUserToPremisess;
                $this->collUserToPremisessPartial = false;
            }
        }

        return $this->collUserToPremisess;
    }

    /**
     * Sets a collection of ChildUserToPremises objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $userToPremisess A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildPremises The current object (for fluent API support)
     */
    public function setUserToPremisess(Collection $userToPremisess, ConnectionInterface $con = null)
    {
        /** @var ChildUserToPremises[] $userToPremisessToDelete */
        $userToPremisessToDelete = $this->getUserToPremisess(new Criteria(), $con)->diff($userToPremisess);


        $this->userToPremisessScheduledForDeletion = $userToPremisessToDelete;

        foreach ($userToPremisessToDelete as $userToPremisesRemoved) {
            $userToPremisesRemoved->setPremises(null);
        }

        $this->collUserToPremisess = null;
        foreach ($userToPremisess as $userToPremises) {
            $this->addUserToPremises($userToPremises);
        }

        $this->collUserToPremisess = $userToPremisess;
        $this->collUserToPremisessPartial = false;

        return $this;
    }

    /**
     * Returns the number of related UserToPremises objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related UserToPremises objects.
     * @throws PropelException
     */
    public function countUserToPremisess(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collUserToPremisessPartial && !$this->isNew();
        if (null === $this->collUserToPremisess || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collUserToPremisess) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getUserToPremisess());
            }

            $query = ChildUserToPremisesQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByPremises($this)
                ->count($con);
        }

        return count($this->collUserToPremisess);
    }

    /**
     * Method called to associate a ChildUserToPremises object to this object
     * through the ChildUserToPremises foreign key attribute.
     *
     * @param  ChildUserToPremises $l ChildUserToPremises
     * @return $this|\Premises The current object (for fluent API support)
     */
    public function addUserToPremises(ChildUserToPremises $l)
    {
        if ($this->collUserToPremisess === null) {
            $this->initUserToPremisess();
            $this->collUserToPremisessPartial = true;
        }

        if (!$this->collUserToPremisess->contains($l)) {
            $this->doAddUserToPremises($l);

            if ($this->userToPremisessScheduledForDeletion and $this->userToPremisessScheduledForDeletion->contains($l)) {
                $this->userToPremisessScheduledForDeletion->remove($this->userToPremisessScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildUserToPremises $userToPremises The ChildUserToPremises object to add.
     */
    protected function doAddUserToPremises(ChildUserToPremises $userToPremises)
    {
        $this->collUserToPremisess[]= $userToPremises;
        $userToPremises->setPremises($this);
    }

    /**
     * @param  ChildUserToPremises $userToPremises The ChildUserToPremises object to remove.
     * @return $this|ChildPremises The current object (for fluent API support)
     */
    public function removeUserToPremises(ChildUserToPremises $userToPremises)
    {
        if ($this->getUserToPremisess()->contains($userToPremises)) {
            $pos = $this->collUserToPremisess->search($userToPremises);
            $this->collUserToPremisess->remove($pos);
            if (null === $this->userToPremisessScheduledForDeletion) {
                $this->userToPremisessScheduledForDeletion = clone $this->collUserToPremisess;
                $this->userToPremisessScheduledForDeletion->clear();
            }
            $this->userToPremisessScheduledForDeletion[]= clone $userToPremises;
            $userToPremises->setPremises(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Premises is new, it will return
     * an empty collection; or if this Premises has previously
     * been saved, it will retrieve related UserToPremisess from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Premises.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildUserToPremises[] List of ChildUserToPremises objects
     */
    public function getUserToPremisessJoinUsersRelatedByTrustUserId(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildUserToPremisesQuery::create(null, $criteria);
        $query->joinWith('UsersRelatedByTrustUserId', $joinBehavior);

        return $this->getUserToPremisess($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Premises is new, it will return
     * an empty collection; or if this Premises has previously
     * been saved, it will retrieve related UserToPremisess from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Premises.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildUserToPremises[] List of ChildUserToPremises objects
     */
    public function getUserToPremisessJoinUsersRelatedByUserRecipient(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildUserToPremisesQuery::create(null, $criteria);
        $query->joinWith('UsersRelatedByUserRecipient', $joinBehavior);

        return $this->getUserToPremisess($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Premises is new, it will return
     * an empty collection; or if this Premises has previously
     * been saved, it will retrieve related UserToPremisess from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Premises.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildUserToPremises[] List of ChildUserToPremises objects
     */
    public function getUserToPremisessJoinUsersRelatedByUserSender(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildUserToPremisesQuery::create(null, $criteria);
        $query->joinWith('UsersRelatedByUserSender', $joinBehavior);

        return $this->getUserToPremisess($query, $con);
    }

    /**
     * Clears the current object, sets all attributes to their default values and removes
     * outgoing references as well as back-references (from other objects to this one. Results probably in a database
     * change of those foreign objects when you call `save` there).
     */
    public function clear()
    {
        if (null !== $this->aUsers) {
            $this->aUsers->removePremises($this);
        }
        if (null !== $this->aRoutes) {
            $this->aRoutes->removePremises($this);
        }
        if (null !== $this->aStatuses) {
            $this->aStatuses->removePremises($this);
        }
        if (null !== $this->aTypesPremise) {
            $this->aTypesPremise->removePremises($this);
        }
        $this->id = null;
        $this->date_sender = null;
        $this->date_recipient = null;
        $this->type_id = null;
        $this->count = null;
        $this->route_id = null;
        $this->cost = null;
        $this->city_sender = null;
        $this->city_recipient = null;
        $this->status_id = null;
        $this->payer_id = null;
        $this->comment = null;
        $this->alreadyInSave = false;
        $this->clearAllReferences();
        $this->resetModified();
        $this->setNew(true);
        $this->setDeleted(false);
    }

    /**
     * Resets all references and back-references to other model objects or collections of model objects.
     *
     * This method is used to reset all php object references (not the actual reference in the database).
     * Necessary for object serialisation.
     *
     * @param      boolean $deep Whether to also clear the references on all referrer objects.
     */
    public function clearAllReferences($deep = false)
    {
        if ($deep) {
            if ($this->collUserToPremisess) {
                foreach ($this->collUserToPremisess as $o) {
                    $o->clearAllReferences($deep);
                }
            }
        } // if ($deep)

        $this->collUserToPremisess = null;
        $this->aUsers = null;
        $this->aRoutes = null;
        $this->aStatuses = null;
        $this->aTypesPremise = null;
    }

    /**
     * Return the string representation of this object
     *
     * @return string
     */
    public function __toString()
    {
        return (string) $this->exportTo(PremisesTableMap::DEFAULT_STRING_FORMAT);
    }

    /**
     * Code to be run before persisting the object
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preSave(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preSave')) {
            return parent::preSave($con);
        }
        return true;
    }

    /**
     * Code to be run after persisting the object
     * @param ConnectionInterface $con
     */
    public function postSave(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postSave')) {
            parent::postSave($con);
        }
    }

    /**
     * Code to be run before inserting to database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preInsert(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preInsert')) {
            return parent::preInsert($con);
        }
        return true;
    }

    /**
     * Code to be run after inserting to database
     * @param ConnectionInterface $con
     */
    public function postInsert(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postInsert')) {
            parent::postInsert($con);
        }
    }

    /**
     * Code to be run before updating the object in database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preUpdate(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preUpdate')) {
            return parent::preUpdate($con);
        }
        return true;
    }

    /**
     * Code to be run after updating the object in database
     * @param ConnectionInterface $con
     */
    public function postUpdate(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postUpdate')) {
            parent::postUpdate($con);
        }
    }

    /**
     * Code to be run before deleting the object in database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preDelete(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preDelete')) {
            return parent::preDelete($con);
        }
        return true;
    }

    /**
     * Code to be run after deleting the object in database
     * @param ConnectionInterface $con
     */
    public function postDelete(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postDelete')) {
            parent::postDelete($con);
        }
    }


    /**
     * Derived method to catches calls to undefined methods.
     *
     * Provides magic import/export method support (fromXML()/toXML(), fromYAML()/toYAML(), etc.).
     * Allows to define default __call() behavior if you overwrite __call()
     *
     * @param string $name
     * @param mixed  $params
     *
     * @return array|string
     */
    public function __call($name, $params)
    {
        if (0 === strpos($name, 'get')) {
            $virtualColumn = substr($name, 3);
            if ($this->hasVirtualColumn($virtualColumn)) {
                return $this->getVirtualColumn($virtualColumn);
            }

            $virtualColumn = lcfirst($virtualColumn);
            if ($this->hasVirtualColumn($virtualColumn)) {
                return $this->getVirtualColumn($virtualColumn);
            }
        }

        if (0 === strpos($name, 'from')) {
            $format = substr($name, 4);

            return $this->importFrom($format, reset($params));
        }

        if (0 === strpos($name, 'to')) {
            $format = substr($name, 2);
            $includeLazyLoadColumns = isset($params[0]) ? $params[0] : true;

            return $this->exportTo($format, $includeLazyLoadColumns);
        }

        throw new BadMethodCallException(sprintf('Call to undefined method: %s.', $name));
    }

}
