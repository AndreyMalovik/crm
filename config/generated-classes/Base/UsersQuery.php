<?php

namespace Base;

use \Users as ChildUsers;
use \UsersQuery as ChildUsersQuery;
use \Exception;
use \PDO;
use Map\UsersTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveQuery\ModelJoin;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'users' table.
 *
 *
 *
 * @method     ChildUsersQuery orderById($order = Criteria::ASC) Order by the id column
 * @method     ChildUsersQuery orderByUsername($order = Criteria::ASC) Order by the username column
 * @method     ChildUsersQuery orderByPhone($order = Criteria::ASC) Order by the phone column
 * @method     ChildUsersQuery orderByRegion($order = Criteria::ASC) Order by the region column
 * @method     ChildUsersQuery orderByPointOfSale($order = Criteria::ASC) Order by the point_of_sale column
 * @method     ChildUsersQuery orderBySpecialization($order = Criteria::ASC) Order by the specialization column
 *
 * @method     ChildUsersQuery groupById() Group by the id column
 * @method     ChildUsersQuery groupByUsername() Group by the username column
 * @method     ChildUsersQuery groupByPhone() Group by the phone column
 * @method     ChildUsersQuery groupByRegion() Group by the region column
 * @method     ChildUsersQuery groupByPointOfSale() Group by the point_of_sale column
 * @method     ChildUsersQuery groupBySpecialization() Group by the specialization column
 *
 * @method     ChildUsersQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildUsersQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildUsersQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildUsersQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildUsersQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildUsersQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildUsersQuery leftJoinPayments($relationAlias = null) Adds a LEFT JOIN clause to the query using the Payments relation
 * @method     ChildUsersQuery rightJoinPayments($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Payments relation
 * @method     ChildUsersQuery innerJoinPayments($relationAlias = null) Adds a INNER JOIN clause to the query using the Payments relation
 *
 * @method     ChildUsersQuery joinWithPayments($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Payments relation
 *
 * @method     ChildUsersQuery leftJoinWithPayments() Adds a LEFT JOIN clause and with to the query using the Payments relation
 * @method     ChildUsersQuery rightJoinWithPayments() Adds a RIGHT JOIN clause and with to the query using the Payments relation
 * @method     ChildUsersQuery innerJoinWithPayments() Adds a INNER JOIN clause and with to the query using the Payments relation
 *
 * @method     ChildUsersQuery leftJoinPremises($relationAlias = null) Adds a LEFT JOIN clause to the query using the Premises relation
 * @method     ChildUsersQuery rightJoinPremises($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Premises relation
 * @method     ChildUsersQuery innerJoinPremises($relationAlias = null) Adds a INNER JOIN clause to the query using the Premises relation
 *
 * @method     ChildUsersQuery joinWithPremises($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Premises relation
 *
 * @method     ChildUsersQuery leftJoinWithPremises() Adds a LEFT JOIN clause and with to the query using the Premises relation
 * @method     ChildUsersQuery rightJoinWithPremises() Adds a RIGHT JOIN clause and with to the query using the Premises relation
 * @method     ChildUsersQuery innerJoinWithPremises() Adds a INNER JOIN clause and with to the query using the Premises relation
 *
 * @method     ChildUsersQuery leftJoinUserToPremisesRelatedByTrustUserId($relationAlias = null) Adds a LEFT JOIN clause to the query using the UserToPremisesRelatedByTrustUserId relation
 * @method     ChildUsersQuery rightJoinUserToPremisesRelatedByTrustUserId($relationAlias = null) Adds a RIGHT JOIN clause to the query using the UserToPremisesRelatedByTrustUserId relation
 * @method     ChildUsersQuery innerJoinUserToPremisesRelatedByTrustUserId($relationAlias = null) Adds a INNER JOIN clause to the query using the UserToPremisesRelatedByTrustUserId relation
 *
 * @method     ChildUsersQuery joinWithUserToPremisesRelatedByTrustUserId($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the UserToPremisesRelatedByTrustUserId relation
 *
 * @method     ChildUsersQuery leftJoinWithUserToPremisesRelatedByTrustUserId() Adds a LEFT JOIN clause and with to the query using the UserToPremisesRelatedByTrustUserId relation
 * @method     ChildUsersQuery rightJoinWithUserToPremisesRelatedByTrustUserId() Adds a RIGHT JOIN clause and with to the query using the UserToPremisesRelatedByTrustUserId relation
 * @method     ChildUsersQuery innerJoinWithUserToPremisesRelatedByTrustUserId() Adds a INNER JOIN clause and with to the query using the UserToPremisesRelatedByTrustUserId relation
 *
 * @method     ChildUsersQuery leftJoinUserToPremisesRelatedByUserRecipient($relationAlias = null) Adds a LEFT JOIN clause to the query using the UserToPremisesRelatedByUserRecipient relation
 * @method     ChildUsersQuery rightJoinUserToPremisesRelatedByUserRecipient($relationAlias = null) Adds a RIGHT JOIN clause to the query using the UserToPremisesRelatedByUserRecipient relation
 * @method     ChildUsersQuery innerJoinUserToPremisesRelatedByUserRecipient($relationAlias = null) Adds a INNER JOIN clause to the query using the UserToPremisesRelatedByUserRecipient relation
 *
 * @method     ChildUsersQuery joinWithUserToPremisesRelatedByUserRecipient($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the UserToPremisesRelatedByUserRecipient relation
 *
 * @method     ChildUsersQuery leftJoinWithUserToPremisesRelatedByUserRecipient() Adds a LEFT JOIN clause and with to the query using the UserToPremisesRelatedByUserRecipient relation
 * @method     ChildUsersQuery rightJoinWithUserToPremisesRelatedByUserRecipient() Adds a RIGHT JOIN clause and with to the query using the UserToPremisesRelatedByUserRecipient relation
 * @method     ChildUsersQuery innerJoinWithUserToPremisesRelatedByUserRecipient() Adds a INNER JOIN clause and with to the query using the UserToPremisesRelatedByUserRecipient relation
 *
 * @method     ChildUsersQuery leftJoinUserToPremisesRelatedByUserSender($relationAlias = null) Adds a LEFT JOIN clause to the query using the UserToPremisesRelatedByUserSender relation
 * @method     ChildUsersQuery rightJoinUserToPremisesRelatedByUserSender($relationAlias = null) Adds a RIGHT JOIN clause to the query using the UserToPremisesRelatedByUserSender relation
 * @method     ChildUsersQuery innerJoinUserToPremisesRelatedByUserSender($relationAlias = null) Adds a INNER JOIN clause to the query using the UserToPremisesRelatedByUserSender relation
 *
 * @method     ChildUsersQuery joinWithUserToPremisesRelatedByUserSender($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the UserToPremisesRelatedByUserSender relation
 *
 * @method     ChildUsersQuery leftJoinWithUserToPremisesRelatedByUserSender() Adds a LEFT JOIN clause and with to the query using the UserToPremisesRelatedByUserSender relation
 * @method     ChildUsersQuery rightJoinWithUserToPremisesRelatedByUserSender() Adds a RIGHT JOIN clause and with to the query using the UserToPremisesRelatedByUserSender relation
 * @method     ChildUsersQuery innerJoinWithUserToPremisesRelatedByUserSender() Adds a INNER JOIN clause and with to the query using the UserToPremisesRelatedByUserSender relation
 *
 * @method     \PaymentsQuery|\PremisesQuery|\UserToPremisesQuery endUse() Finalizes a secondary criteria and merges it with its primary Criteria
 *
 * @method     ChildUsers findOne(ConnectionInterface $con = null) Return the first ChildUsers matching the query
 * @method     ChildUsers findOneOrCreate(ConnectionInterface $con = null) Return the first ChildUsers matching the query, or a new ChildUsers object populated from the query conditions when no match is found
 *
 * @method     ChildUsers findOneById(string $id) Return the first ChildUsers filtered by the id column
 * @method     ChildUsers findOneByUsername(string $username) Return the first ChildUsers filtered by the username column
 * @method     ChildUsers findOneByPhone(string $phone) Return the first ChildUsers filtered by the phone column
 * @method     ChildUsers findOneByRegion(string $region) Return the first ChildUsers filtered by the region column
 * @method     ChildUsers findOneByPointOfSale(string $point_of_sale) Return the first ChildUsers filtered by the point_of_sale column
 * @method     ChildUsers findOneBySpecialization(string $specialization) Return the first ChildUsers filtered by the specialization column *

 * @method     ChildUsers requirePk($key, ConnectionInterface $con = null) Return the ChildUsers by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildUsers requireOne(ConnectionInterface $con = null) Return the first ChildUsers matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildUsers requireOneById(string $id) Return the first ChildUsers filtered by the id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildUsers requireOneByUsername(string $username) Return the first ChildUsers filtered by the username column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildUsers requireOneByPhone(string $phone) Return the first ChildUsers filtered by the phone column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildUsers requireOneByRegion(string $region) Return the first ChildUsers filtered by the region column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildUsers requireOneByPointOfSale(string $point_of_sale) Return the first ChildUsers filtered by the point_of_sale column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildUsers requireOneBySpecialization(string $specialization) Return the first ChildUsers filtered by the specialization column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildUsers[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildUsers objects based on current ModelCriteria
 * @method     ChildUsers[]|ObjectCollection findById(string $id) Return ChildUsers objects filtered by the id column
 * @method     ChildUsers[]|ObjectCollection findByUsername(string $username) Return ChildUsers objects filtered by the username column
 * @method     ChildUsers[]|ObjectCollection findByPhone(string $phone) Return ChildUsers objects filtered by the phone column
 * @method     ChildUsers[]|ObjectCollection findByRegion(string $region) Return ChildUsers objects filtered by the region column
 * @method     ChildUsers[]|ObjectCollection findByPointOfSale(string $point_of_sale) Return ChildUsers objects filtered by the point_of_sale column
 * @method     ChildUsers[]|ObjectCollection findBySpecialization(string $specialization) Return ChildUsers objects filtered by the specialization column
 * @method     ChildUsers[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class UsersQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \Base\UsersQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'crm', $modelName = '\\Users', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildUsersQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildUsersQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildUsersQuery) {
            return $criteria;
        }
        $query = new ChildUsersQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildUsers|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(UsersTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = UsersTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildUsers A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT id, username, phone, region, point_of_sale, specialization FROM users WHERE id = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildUsers $obj */
            $obj = new ChildUsers();
            $obj->hydrate($row);
            UsersTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildUsers|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildUsersQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(UsersTableMap::COL_ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildUsersQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(UsersTableMap::COL_ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE id = 1234
     * $query->filterById(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE id > 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildUsersQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(UsersTableMap::COL_ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(UsersTableMap::COL_ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(UsersTableMap::COL_ID, $id, $comparison);
    }

    /**
     * Filter the query on the username column
     *
     * Example usage:
     * <code>
     * $query->filterByUsername('fooValue');   // WHERE username = 'fooValue'
     * $query->filterByUsername('%fooValue%', Criteria::LIKE); // WHERE username LIKE '%fooValue%'
     * </code>
     *
     * @param     string $username The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildUsersQuery The current query, for fluid interface
     */
    public function filterByUsername($username = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($username)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(UsersTableMap::COL_USERNAME, $username, $comparison);
    }

    /**
     * Filter the query on the phone column
     *
     * Example usage:
     * <code>
     * $query->filterByPhone('fooValue');   // WHERE phone = 'fooValue'
     * $query->filterByPhone('%fooValue%', Criteria::LIKE); // WHERE phone LIKE '%fooValue%'
     * </code>
     *
     * @param     string $phone The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildUsersQuery The current query, for fluid interface
     */
    public function filterByPhone($phone = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($phone)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(UsersTableMap::COL_PHONE, $phone, $comparison);
    }

    /**
     * Filter the query on the region column
     *
     * Example usage:
     * <code>
     * $query->filterByRegion('fooValue');   // WHERE region = 'fooValue'
     * $query->filterByRegion('%fooValue%', Criteria::LIKE); // WHERE region LIKE '%fooValue%'
     * </code>
     *
     * @param     string $region The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildUsersQuery The current query, for fluid interface
     */
    public function filterByRegion($region = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($region)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(UsersTableMap::COL_REGION, $region, $comparison);
    }

    /**
     * Filter the query on the point_of_sale column
     *
     * Example usage:
     * <code>
     * $query->filterByPointOfSale('fooValue');   // WHERE point_of_sale = 'fooValue'
     * $query->filterByPointOfSale('%fooValue%', Criteria::LIKE); // WHERE point_of_sale LIKE '%fooValue%'
     * </code>
     *
     * @param     string $pointOfSale The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildUsersQuery The current query, for fluid interface
     */
    public function filterByPointOfSale($pointOfSale = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($pointOfSale)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(UsersTableMap::COL_POINT_OF_SALE, $pointOfSale, $comparison);
    }

    /**
     * Filter the query on the specialization column
     *
     * Example usage:
     * <code>
     * $query->filterBySpecialization('fooValue');   // WHERE specialization = 'fooValue'
     * $query->filterBySpecialization('%fooValue%', Criteria::LIKE); // WHERE specialization LIKE '%fooValue%'
     * </code>
     *
     * @param     string $specialization The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildUsersQuery The current query, for fluid interface
     */
    public function filterBySpecialization($specialization = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($specialization)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(UsersTableMap::COL_SPECIALIZATION, $specialization, $comparison);
    }

    /**
     * Filter the query by a related \Payments object
     *
     * @param \Payments|ObjectCollection $payments the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildUsersQuery The current query, for fluid interface
     */
    public function filterByPayments($payments, $comparison = null)
    {
        if ($payments instanceof \Payments) {
            return $this
                ->addUsingAlias(UsersTableMap::COL_ID, $payments->getUserId(), $comparison);
        } elseif ($payments instanceof ObjectCollection) {
            return $this
                ->usePaymentsQuery()
                ->filterByPrimaryKeys($payments->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByPayments() only accepts arguments of type \Payments or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Payments relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildUsersQuery The current query, for fluid interface
     */
    public function joinPayments($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Payments');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Payments');
        }

        return $this;
    }

    /**
     * Use the Payments relation Payments object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \PaymentsQuery A secondary query class using the current class as primary query
     */
    public function usePaymentsQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinPayments($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Payments', '\PaymentsQuery');
    }

    /**
     * Filter the query by a related \Premises object
     *
     * @param \Premises|ObjectCollection $premises the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildUsersQuery The current query, for fluid interface
     */
    public function filterByPremises($premises, $comparison = null)
    {
        if ($premises instanceof \Premises) {
            return $this
                ->addUsingAlias(UsersTableMap::COL_ID, $premises->getPayerId(), $comparison);
        } elseif ($premises instanceof ObjectCollection) {
            return $this
                ->usePremisesQuery()
                ->filterByPrimaryKeys($premises->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByPremises() only accepts arguments of type \Premises or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Premises relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildUsersQuery The current query, for fluid interface
     */
    public function joinPremises($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Premises');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Premises');
        }

        return $this;
    }

    /**
     * Use the Premises relation Premises object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \PremisesQuery A secondary query class using the current class as primary query
     */
    public function usePremisesQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinPremises($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Premises', '\PremisesQuery');
    }

    /**
     * Filter the query by a related \UserToPremises object
     *
     * @param \UserToPremises|ObjectCollection $userToPremises the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildUsersQuery The current query, for fluid interface
     */
    public function filterByUserToPremisesRelatedByTrustUserId($userToPremises, $comparison = null)
    {
        if ($userToPremises instanceof \UserToPremises) {
            return $this
                ->addUsingAlias(UsersTableMap::COL_ID, $userToPremises->getTrustUserId(), $comparison);
        } elseif ($userToPremises instanceof ObjectCollection) {
            return $this
                ->useUserToPremisesRelatedByTrustUserIdQuery()
                ->filterByPrimaryKeys($userToPremises->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByUserToPremisesRelatedByTrustUserId() only accepts arguments of type \UserToPremises or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the UserToPremisesRelatedByTrustUserId relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildUsersQuery The current query, for fluid interface
     */
    public function joinUserToPremisesRelatedByTrustUserId($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('UserToPremisesRelatedByTrustUserId');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'UserToPremisesRelatedByTrustUserId');
        }

        return $this;
    }

    /**
     * Use the UserToPremisesRelatedByTrustUserId relation UserToPremises object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \UserToPremisesQuery A secondary query class using the current class as primary query
     */
    public function useUserToPremisesRelatedByTrustUserIdQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinUserToPremisesRelatedByTrustUserId($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'UserToPremisesRelatedByTrustUserId', '\UserToPremisesQuery');
    }

    /**
     * Filter the query by a related \UserToPremises object
     *
     * @param \UserToPremises|ObjectCollection $userToPremises the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildUsersQuery The current query, for fluid interface
     */
    public function filterByUserToPremisesRelatedByUserRecipient($userToPremises, $comparison = null)
    {
        if ($userToPremises instanceof \UserToPremises) {
            return $this
                ->addUsingAlias(UsersTableMap::COL_ID, $userToPremises->getUserRecipient(), $comparison);
        } elseif ($userToPremises instanceof ObjectCollection) {
            return $this
                ->useUserToPremisesRelatedByUserRecipientQuery()
                ->filterByPrimaryKeys($userToPremises->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByUserToPremisesRelatedByUserRecipient() only accepts arguments of type \UserToPremises or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the UserToPremisesRelatedByUserRecipient relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildUsersQuery The current query, for fluid interface
     */
    public function joinUserToPremisesRelatedByUserRecipient($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('UserToPremisesRelatedByUserRecipient');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'UserToPremisesRelatedByUserRecipient');
        }

        return $this;
    }

    /**
     * Use the UserToPremisesRelatedByUserRecipient relation UserToPremises object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \UserToPremisesQuery A secondary query class using the current class as primary query
     */
    public function useUserToPremisesRelatedByUserRecipientQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinUserToPremisesRelatedByUserRecipient($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'UserToPremisesRelatedByUserRecipient', '\UserToPremisesQuery');
    }

    /**
     * Filter the query by a related \UserToPremises object
     *
     * @param \UserToPremises|ObjectCollection $userToPremises the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildUsersQuery The current query, for fluid interface
     */
    public function filterByUserToPremisesRelatedByUserSender($userToPremises, $comparison = null)
    {
        if ($userToPremises instanceof \UserToPremises) {
            return $this
                ->addUsingAlias(UsersTableMap::COL_ID, $userToPremises->getUserSender(), $comparison);
        } elseif ($userToPremises instanceof ObjectCollection) {
            return $this
                ->useUserToPremisesRelatedByUserSenderQuery()
                ->filterByPrimaryKeys($userToPremises->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByUserToPremisesRelatedByUserSender() only accepts arguments of type \UserToPremises or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the UserToPremisesRelatedByUserSender relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildUsersQuery The current query, for fluid interface
     */
    public function joinUserToPremisesRelatedByUserSender($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('UserToPremisesRelatedByUserSender');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'UserToPremisesRelatedByUserSender');
        }

        return $this;
    }

    /**
     * Use the UserToPremisesRelatedByUserSender relation UserToPremises object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \UserToPremisesQuery A secondary query class using the current class as primary query
     */
    public function useUserToPremisesRelatedByUserSenderQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinUserToPremisesRelatedByUserSender($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'UserToPremisesRelatedByUserSender', '\UserToPremisesQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   ChildUsers $users Object to remove from the list of results
     *
     * @return $this|ChildUsersQuery The current query, for fluid interface
     */
    public function prune($users = null)
    {
        if ($users) {
            $this->addUsingAlias(UsersTableMap::COL_ID, $users->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the users table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(UsersTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            UsersTableMap::clearInstancePool();
            UsersTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(UsersTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(UsersTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            UsersTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            UsersTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

} // UsersQuery
