<?php

use Base\TypesPremise as BaseTypesPremise;

/**
 * Skeleton subclass for representing a row from the 'types_premise' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class TypesPremise extends BaseTypesPremise
{

}
