
# This is a fix for InnoDB in MySQL >= 4.1.x
# It "suspends judgement" for fkey relationships until are tables are set.
SET FOREIGN_KEY_CHECKS = 0;

-- ---------------------------------------------------------------------
-- payments
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `payments`;

CREATE TABLE `payments`
(
    `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
    `user_id` bigint(20) unsigned NOT NULL,
    `amount` INTEGER NOT NULL,
    PRIMARY KEY (`id`),
    INDEX `user_payments` (`user_id`),
    CONSTRAINT `user_payments`
        FOREIGN KEY (`user_id`)
        REFERENCES `users` (`id`)
) ENGINE=InnoDB;

-- ---------------------------------------------------------------------
-- premises
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `premises`;

CREATE TABLE `premises`
(
    `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
    `date_sender` bigint(20) unsigned NOT NULL,
    `date_recipient` INTEGER NOT NULL,
    `type_id` tinyint(3) unsigned NOT NULL,
    `count` tinyint(3) unsigned NOT NULL,
    `route_id` bigint(20) unsigned NOT NULL,
    `cost` int(10) unsigned NOT NULL,
    `city_sender` VARCHAR(255) NOT NULL,
    `city_recipient` VARCHAR(255) NOT NULL,
    `status_id` tinyint(3) unsigned NOT NULL,
    `payer_id` bigint(20) unsigned NOT NULL,
    `comment` TEXT NOT NULL,
    PRIMARY KEY (`id`),
    INDEX `premises_route` (`route_id`),
    INDEX `premises_statuse` (`status_id`),
    INDEX `premises_payer` (`payer_id`),
    INDEX `premises_type` (`type_id`),
    CONSTRAINT `premises_payer`
        FOREIGN KEY (`payer_id`)
        REFERENCES `users` (`id`),
    CONSTRAINT `premises_route`
        FOREIGN KEY (`route_id`)
        REFERENCES `routes` (`id`),
    CONSTRAINT `premises_statuse`
        FOREIGN KEY (`status_id`)
        REFERENCES `statuses` (`id`),
    CONSTRAINT `premises_type`
        FOREIGN KEY (`type_id`)
        REFERENCES `types_premise` (`id`)
) ENGINE=InnoDB;

-- ---------------------------------------------------------------------
-- routes
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `routes`;

CREATE TABLE `routes`
(
    `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
    `name` VARCHAR(255) NOT NULL,
    PRIMARY KEY (`id`)
) ENGINE=InnoDB;

-- ---------------------------------------------------------------------
-- statuses
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `statuses`;

CREATE TABLE `statuses`
(
    `id` tinyint(3) unsigned NOT NULL AUTO_INCREMENT,
    `name` VARCHAR(255) NOT NULL,
    PRIMARY KEY (`id`)
) ENGINE=InnoDB;

-- ---------------------------------------------------------------------
-- trust_to_user_premise
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `trust_to_user_premise`;

CREATE TABLE `trust_to_user_premise`
(
    `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
    `user_id` bigint(20) unsigned NOT NULL,
    `premise_id` bigint(20) unsigned NOT NULL,
    PRIMARY KEY (`id`)
) ENGINE=InnoDB;

-- ---------------------------------------------------------------------
-- types_premise
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `types_premise`;

CREATE TABLE `types_premise`
(
    `id` tinyint(3) unsigned NOT NULL AUTO_INCREMENT,
    `name` VARCHAR(255) NOT NULL,
    PRIMARY KEY (`id`)
) ENGINE=InnoDB;

-- ---------------------------------------------------------------------
-- user_to_premises
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `user_to_premises`;

CREATE TABLE `user_to_premises`
(
    `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
    `user_sender` bigint(20) unsigned NOT NULL,
    `premise_id` bigint(20) unsigned NOT NULL,
    `user_recipient` bigint(20) unsigned NOT NULL,
    `trust_user_id` bigint(20) unsigned,
    PRIMARY KEY (`id`),
    INDEX `user_sender` (`user_sender`),
    INDEX `user_recipient` (`user_recipient`),
    INDEX `user_premise` (`premise_id`),
    INDEX `user_premise_trust` (`trust_user_id`),
    CONSTRAINT `user_premise`
        FOREIGN KEY (`premise_id`)
        REFERENCES `premises` (`id`),
    CONSTRAINT `user_premise_trust`
        FOREIGN KEY (`trust_user_id`)
        REFERENCES `users` (`id`),
    CONSTRAINT `user_recipient`
        FOREIGN KEY (`user_recipient`)
        REFERENCES `users` (`id`),
    CONSTRAINT `user_sender`
        FOREIGN KEY (`user_sender`)
        REFERENCES `users` (`id`)
) ENGINE=InnoDB;

-- ---------------------------------------------------------------------
-- users
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users`
(
    `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
    `username` VARCHAR(255) NOT NULL,
    `phone` VARCHAR(255) NOT NULL,
    `region` VARCHAR(255) NOT NULL,
    `point_of_sale` VARCHAR(255) NOT NULL,
    `specialization` TEXT NOT NULL,
    PRIMARY KEY (`id`)
) ENGINE=InnoDB;

# This restores the fkey checks, after having unset them earlier
SET FOREIGN_KEY_CHECKS = 1;
